<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!--Page de modification des paramètres : editer profile + lien vers modifier mdp-->
<head>
<title>Paramètres du compte</title>
</head>
<?php
require_once 'header.php';

$query=mysqli_query($con,"SELECT * FROM membres WHERE email='$email'");
$row = $query->fetch_array(MYSQLI_ASSOC);	


 if( isset( $_POST['action'] ) && $_POST['action'] == 'statut' )
  {
	  $statupterso = htmlspecialchars($_POST['statupterso']);
	   $query2=mysqli_query($con,"UPDATE membres SET statut='$statupterso' WHERE email='$email'");
		echo "<script>window.location.href='/home.php?statut=';</script>";
  }

if (isset($_POST['nom']))
	{
	  if(!empty($_POST['nom'])) 
	  {
		  $nom = htmlspecialchars($_POST['nom']);
		  $prenom = htmlspecialchars($_POST['prenom']);
		  $email = htmlspecialchars($_POST['email']);
		  $ddn = htmlspecialchars($_POST['datetimepicker']);
		  $tel = htmlspecialchars($_POST['tel']);
		  $descri = htmlspecialchars($_POST['descri']);
		  $lieu = htmlspecialchars($_POST['lieu']);
		  $hobbies = htmlspecialchars($_POST['hob']);
		  $pays = htmlspecialchars($_POST['pays']);
		  $musique = htmlspecialchars($_POST['musique']);
		  $serie = htmlspecialchars($_POST['series']);
		  
		  
		  $query2=mysqli_query($con,"UPDATE membres SET nom='$nom',prenom='$prenom',email='$email',datetimepicker='$ddn',tel='$tel',description='$descri',ldn='$lieu',hobbies='$hobbies',pays='$pays', musique='$musique', serie='$serie' WHERE email='$email'");
		   echo "<meta http-equiv='refresh' content='0'>"; //mettre a jour info de la page apres l'update
		  
	  }
	}

?>



<!DOCTYPE html>
<html lang="fr">
<head>
	<style>
	.texteviolet{color: #4A089B;}
	</style>
	

	<link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-select.css">
</head>
<body>





<!--  En-tête principal Votre compte -->

<div class="main-header">
	<div class="content-bg-wrap">
		<div class="content-bg bg-account"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 m-auto col-md-8 col-sm-12 col-xs-12">
				<div class="main-header-content"><br/><br/>
					<h1 class="texteviolet">Le tableau de bord de votre compte</h1>
					<p class="texteviolet">Bienvenue dans le tableau de bord de votre compte! 
					Ici vous trouverez tout ce dont vous avez besoin pour changer vos informations de profil, paramètres,
changez votre mot de passe et bien plus encore! Amusez-vous bien!
					</p><br/><br/>
				</div>
			</div>
		</div>

<!-- ... fin  En-tête principal Votre compte -->


		<!-- Informations personnelles -->

		<div class="container">
			<div class="row">
				<div class="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
					<div class="ui-block">
						<div class="ui-block-title">
							<h6 class="title">Infos persos</h6>
						</div>
						<div class="ui-block-content">
							<form method="POST" action="parametres.php"> 
								<div class="row">

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">Nom</label>
											<input class="form-control" placeholder="" type="text" value="<?php echo $row['nom'];?>" name="nom" >
										</div>

										<div class="form-group label-floating">
											<label class="control-label">Email</label>
											<input class="form-control" placeholder="" type="email" name="email" value="<?php echo $row['email'];?>">
										</div>

										<div class="form-group date-time-picker label-floating">
											<label class="control-label">Anniversaire</label>
											<input name="datetimepicker" value="<?php echo $row['datetimepicker'];?>" />
											<span class="input-group-addon">
												<svg class="month-calendar-icon icon"><use xlink:href="icons/icons.svg#month-calendar-icon"></use></svg>
											</span>
										</div>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">Prenom</label>
											<input class="form-control" placeholder="" name="prenom" type="text" value="<?php echo $row['prenom'];?>">
										</div>

										<div class="form-group label-floating">
											<label class="control-label">Telephone</label>
											<input class="form-control" placeholder="" name="tel" type="text" value="<?php echo $row['tel'];?>">
										</div>
										
										<div class="form-group label-floating">
											<label class="control-label">Lieu de naissance</label>
											<input class="form-control" placeholder="" name="lieu" type="text" value="<?php echo $row['ldn'];?>">
										</div>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">Description</label>
											<textarea class="form-control" placeholder="" name="descri" value=""><?php echo $row['description'];?></textarea>
										</div>
										
										<div class="form-group label-floating">
											<label class="control-label">Musiques/Artistes</label>
											<textarea class="form-control" placeholder="" name="musique" value=""><?php echo $row['musique'];?></textarea>
										</div>
										
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating is-select">
											<label class="control-label">Pays</label>
											<select name="pays" class="selectpicker form-control" size="auto">
												<option value="base"><?php echo $row['pays'];?></option>
												<option value="France">France</option>
												<option value="United States">United States</option>
												<option value="Australia">Australia</option>
											</select>
										</div>
									
										
										
										<div class="form-group label-floating">
											<label class="control-label">Séries/TV</label>
											<textarea class="form-control" placeholder="" name="series" value=""><?php echo $row['serie'];?></textarea>
										</div>
										
										<div class="form-group label-floating">
											<label class="control-label">Hobbies</label>
											<input class="form-control" placeholder="" name="hob" type="text" value="<?php echo $row['hobbies'];?>">
										</div>
										
									</div>
										
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<button class="btn btn-primary btn-lg full-width">Sauvegarder</button>
									</div>

								</div>
							</form>
						</div>
					</div>
				</div>

				<div class="col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12 responsive-display-none">
					<div class="ui-block">
						<div class="your-profile">
							<div class="ui-block-title ui-block-title-small">
								<h6 class="title">Votre profil</h6>
							</div>

							<div id="accordion" role="tablist" aria-multiselectable="true">
								<div class="card">
									<div class="card-header" role="tab" id="headingOne">
										<h6 class="mb-0">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
												Editer profil
												<svg class="dropdown-arrow-icon"><use xlink:href="icons/icons.svg#dropdown-arrow-icon"></use></svg>
											</a>
										</h6>
									</div>

									<div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
										<ul class="your-profile-menu">
											<li>

												<a href="/parametres.php">Infos Perso</a>
											</li>
											
											<li>
												<a href="/changermdp.php">Mot de Passe</a>
											</li>
											
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ... fin informations personnelles -->





<script src="js/moment.min.js"></script>
<script src="js/daterangepicker.min.js"></script>

</body>
</html>
