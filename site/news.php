<?php

  require_once 'header.php';

 

?>

<!DOCTYPE html>
<html lang="en">
<div class="header-spacer"></div>
	<link rel="stylesheet" type="text/css" href="css/bootstrap-select.css">

 <style type="text/css">
    #upload{
    display: none;
}
    #uploadpubli{
    display: none;
}


.typeahead-devs, .tt-hint,.recherche_publi {
    border: 2px solid #CCCCCC;
    border-radius: 8px 8px 8px 8px;
    font-size: 24px;
    height: 45px;
    line-height: 30px;
    outline: medium none;
    padding: 8px 12px;
    width: 300px;
}


 </style>
<script>
$(document).ready(function() {

$('input.recherche_publi').typeahead({
  name: 'recherche_publi',
  remote : 'search.php?publication=%QUERY'
});

});
</script>
<!--Contenu principal -->
<div class="container">
	<div class="row">


		<!--Publications -->
		<div  class="col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-xs-12">
			<div id="newsfeed-items-grid">
				<div class="ui-block">
					<article class="hentry post">
						<form method="POST" action="img.php" enctype="multipart/form-data">  
						<input type="hidden" name="auteur" value="<?php echo $userstr; ?>" />
						<input type="hidden" name="auteurid" value="<?php echo $id_log; ?>" />
						<input type="hidden" name="idpage" value="<?php echo $id_log; ?>" />
						
						<textarea class="form-control" placeholder="Contenu de la publication" name="contenu" value=""></textarea>

						<div class="form-group label-floating is-select">
										<label class="control-label">Confidentialité</label>
										<select class="selectpicker form-control" name="confidentialite" size="auto">
											<option value="1">Publique</option>
											<option value="2">Amis</option>
											<option value="3">Privée</option>
										</select>
						</div>

						<input id="uploadpubli" name="uploadpubli" type="file"/>
						<li>
						
							<a href="#" id="upload_publi" name="upload_publi" >Ajouter une photo à la publication</a>
						</li><br/>
						<input type="submit"  name="action" value="poster" class="btn btn-purple btn-lg full-width" value="Poster la publication" />
						</form>								  
					</article>
				</div>
			<form method="POST" action="recherche.php" >
				<div class="button">
     				<input type="text" name="recherche_publi" id ="recherche_publi" class="recherche_publi" autocomplete="off" spellcheck="false" placeholder="Rechercher une publi..">	
					<button>
					<svg class="magnifying-glass-icon"><use xlink:href="icons/icons.svg#magnifying-glass-icon"></use></svg>
					</button>
				</div>  
				</div>
			</form>
			

			<a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid"><svg class="three-dots-icon"><use xlink:href="icons/icons.svg#three-dots-icon"></use></svg></a>
		 	
		</div>
		
		
		
	<?php

					
	
	$query=mysqli_query($con,"SELECT * FROM publications ORDER BY time DESC");
	$num = $query->num_rows;
 
	for ($j = 0 ; $j < $num ; ++$j)
    {

      $row = $query->fetch_array(MYSQLI_ASSOC);	
	  $publi = $row['id_publication'];
	  $auteur = $row['id_auteur'];
	 	$confidentialite = $row['confiden'];
	  
	  $info_personne = mysqli_query($con,"SELECT * FROM membres WHERE id='$auteur'");
	  
	  $infospersonne = $info_personne->fetch_array(MYSQLI_ASSOC);	
	  
	  $check_likes = mysqli_query($con,"SELECT id FROM likes WHERE id_publi='$publi' AND id_membre ='$id_log'");
	  $listlike = mysqli_query($con,"SELECT id_membre FROM likes WHERE id_publi='$publi'");
	  $listdislike = mysqli_query($con,"SELECT id_membre FROM dislikes WHERE id_publi='$publi'");
		  
	  $check_dislikes = mysqli_query($con,"SELECT id FROM dislikes WHERE id_publi='$publi' AND id_membre ='$id_log'");
	  $bool_like= $check_likes->num_rows;
	  $bool_dislike= $check_dislikes->num_rows;

	
	  $likes = mysqli_query($con,"SELECT id FROM likes WHERE id_publi='$publi'");
	  $likes = $likes->num_rows;
	 
	  $dislikes = mysqli_query($con,"SELECT id FROM dislikes WHERE id_publi='$publi'");
	  $dislikes = $dislikes->num_rows;

	  $estamis=mysqli_query($con,"SELECT * FROM amis WHERE id_1='$auteur' AND id_2='$id_log' OR id_1='$id_log' AND id_2='$auteur'");
	  $estamisnum = $estamis->num_rows;


	  if ($confidentialite == '1' || $confidentialite == '2' && $estamisnum == 1 || $auteur == $id_log)
	  {
	  
	?>

	 
 <div class="modal fade" id="modal<?php echo $j;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Likes</h4>
            </div>
            <div class="modal-body">
				<ul class="list-group">
				<?php
                     while ($mem = mysqli_fetch_assoc($listlike)):
							$idlist = $mem['id_membre'];
							$listquery = mysqli_query($con,"SELECT nom,prenom,id FROM membres WHERE id='$idlist'");
							$listnom = $listquery->fetch_array(MYSQLI_ASSOC);
							echo '<a href="/test2.php?id='.$listnom['id'].'"><li class="list-group-item">'.$listnom['nom']. ' ' .$listnom['prenom'].'</li></a>';
							endwhile;		
				?>
				</ul>
            </div><!--/modal-body-->
		</div>
    </div>
</div>

	<div class="modal fade" id="modald<?php echo $j;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Dislikes</h4>
                </div>
                <div class="modal-body">
					<ul class="list-group">
						<?php
                                while ($mem = mysqli_fetch_assoc($listdislike)):
									$idlistd = $mem['id_membre'];
									
									$listqueryd = mysqli_query($con,"SELECT nom,prenom,id FROM membres WHERE id='$idlistd'");
									$listnomd = $listqueryd->fetch_array(MYSQLI_ASSOC);
									echo '<a href="/test2.php?id='.$listnomd['id'].'"><li class="list-group-item">'.$listnomd['nom']. ' ' .$listnomd['prenom'].'</li></a>';
									endwhile;		
						?>
					</ul>
                </div><!--/modal-body-->
            </div>
        </div>
    </div>
				  

	<div class="col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<article class="hentry post">
					<div class="post__author author vcard inline-items">
							<img width="42" height="42" src="<?php echo $infospersonne['avatar'];?>" alt="author">
							<div class="author-date">
								<a class="h6 post__author-name fn" href="02-ProfilePage.html"><?php echo $row['auteur'];?></a>
								<div class="post__date">
									<time class="published" datetime="2017-03-24T18:18">
										<?php echo date('Y-m-d H:i:s', $row['time'] + 21600); //heure fr ?>
									</time>
								</div>
							</div>

							<?php if ( $row['id_auteur'] == $id_log)
							{
								?>
							<div class="more"><svg class="three-dots-icon"><use xlink:href="icons/icons.svg#three-dots-icon"></use></svg>
								<ul class="more-dropdown">
									<li>
										<a href="modifier.php?contenu=<?php echo $row['contenu'];?>&idpubli=<?php echo $row['id_publication'];?>&auteurid=<?php echo $row['id_auteur'];?>&monid=<?php echo $id_log;?>">Modifier la publication </a>
									</li>
									
									<li>
										<a href="publications.php?action=delete&idpubli=<?php echo $row['id_publication'];?>&auteurid=<?php echo $row['id_auteur'];?>&monid=<?php echo $id_log;?>" name="action" value="delete">Supprimer la publication</a>
									</li>
									
								</ul>
							</div>
							<?php
							}
							?>

					</div>

					<p> <?php echo $row['contenu'];?> </p>
					
					<?php if (!empty($row['image']))
					{
					?>	
					
					<ul class="widget w-last-photo js-zoom-gallery">
					<a href="<?php echo $row['image'];?>">
								<img style='height: 50%; width: 50%; object-fit: contain' src="<?php echo $row['image'];?>">
							</a>
						</ul>
						<br/>
					
						
						<?php 
					}
					?>




						<div class="post-additional-info inline-items">
						
							
								
									<a href="aimer.php?t=1&id_perso=<?php echo $id_log;?>&id=<?php echo $row['id_publication'];?>"> <?php if ($bool_like == 1){echo "Je n'aime plus";}else{ echo "J'aime";} ?></a><a  href="#modal<?php echo $j;?>" data-toggle="modal" data-target="#modal<?php echo $j;?>"><?php echo $likes ?></a>
								<a href="aimer.php?t=2&id_perso=<?php echo $id_log;?>&id=<?php echo $row['id_publication'];?>"> <?php if ($bool_dislike == 1){echo "Retirer dislike";}else{ echo "Je n'aime pas";} ?></a><a  href="#modald<?php echo $j;?>" data-toggle="modal" data-target="#modald<?php echo $j;?>"><?php echo $dislikes ?></a>
							
							
						<?php 
							$idpubli = $row['id_publication'];
							$query2=mysqli_query($con,"SELECT * FROM commentaires WHERE id_publi_com='$idpubli'");

							$num2 = $query2->num_rows;
							?>
							
							<div class="comments-shared">
								<a href="#" class="post-add-icon inline-items">
									<svg class="speech-balloon-icon"><use xlink:href="icons/icons.svg#speech-balloon-icon"></use></svg>
									<span><?php echo $num2; ?></span>
								</a>	
							</div>

							</div>
							
						

							</article>

							<ul class="comments-list">
							<?php 
								for ($h = 0 ; $h < $num2 ; ++$h)
								{
								$row2 = $query2->fetch_array(MYSQLI_ASSOC);	
								$publi2 = $row2['id_commentaire'];
								$idauter = $row2['id_auteur'];
									$avatarquery=mysqli_query($con,"SELECT avatar FROM membres WHERE id='$idauter'");
									$avatar = $avatarquery->fetch_array(MYSQLI_ASSOC);	
							?>


							
								<li>
									<div class="post__author author vcard inline-items">
										<img src="<?php echo $avatar['avatar'];?>" alt="author">

										<div class="author-date">
											<a class="h6 post__author-name fn" href="#"><?php echo $row2['auteur'];?></a>
											<div class="post__date">
												<time class="published" datetime="2017-03-24T18:18">
												<?php echo date('Y-m-d H:i:s', $row2['time'] + 21600); //heure fr ?>
												</time>
											</div>
										</div>>

									</div>

									<p> <?php echo $row2['commentaire'];?> </p>

								</li>
							
							

							
<?php
						}
						?>
<ul/>
						
							 <!-- Commentaires -->
							<form method="POST" action="commentaire.php">
								<input type="hidden" name="auteur" value="<?php echo $userstr; ?>" />
								<input type="hidden" name="auteurid" value="<?php echo $id_log; ?>" />
								<input type="hidden" name="idpubli" value="<?php echo $row['id_publication']; ?>" />
								<textarea class="form-control" name="commentaire" placeholder="Exprimez-vous..." value="" ></textarea><br/>
								<input type="submit" value="Poster votre commentaire" class="btn btn-primary" name="submit_commentaire" />
							</form>
						

						
							
								

		</div> 	
			</div>
	
<?php
}

}
?>

<script>


 $(document).ready(function(){
    $("#upload_publi").on('click', function(e){
        e.preventDefault();
        $("#uploadpubli:hidden").trigger('click');
    });
});
</script>
</body>
</html>