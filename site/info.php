<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!--Page d'informations sur l'utilisateur, possibilité d'exporter un fichier xml dans le cas où l'utilisateur est amis avec nous-->
<head>
<title>Informations</title>
</head>
<?php
require_once 'header.php';

if (isset($_GET['id']))
{
    $idpage = $_GET['id'];

    $query = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,sexe,description,hobbies, pays, musique, serie, statut  FROM membres WHERE id='$idpage'");
    $row = $query->fetch_array(MYSQLI_ASSOC);
?>



<!DOCTYPE html>
<html lang="en">
<head>
	<style>
	.texteviolet{color: #4A089B;}
	</style>
	

	<link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-select.css">
</head>
<body>





<!-- En-tête principal Votre compte -->

<div class="main-header">
		<div class="content-bg-wrap">
			<div class="content-bg bg-account"></div>
		</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 m-auto col-md-8 col-sm-12 col-xs-12">
				<div class="main-header-content"><br/><br/>
					<h1 class="texteviolet"><b>Vos Informations personnelles </b></h1>
					<p class="texteviolet">Retrouvez ici de nombreuses informations intéréssantes ! 
					</p>
				</div>
			</div>
		</div>


		<!-- ... fin de l'en-tête principal Votre compte -->
		<?php
    $estamis = mysqli_query($con, "SELECT * FROM amis WHERE id_1='$idpage' AND id_2='$id_log' OR id_1='$id_log' AND id_2='$idpage'");
    $estamisnum = $estamis->num_rows;

    if ($estamisnum == 1 || $idpage == $id_log)
    {

?>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 m-auto col-md-8 col-sm-12 col-xs-12">
						<div class="main-header-content"><br/><br/>
							<p class="texteviolet">Vous pouvez télécharger un fichier au format xml, contenant ces informations <br/>
							(faites de même sur la page d'informations de vos amis)
							</p>
							<li>
								<a href="export.php?id=<?php echo $idpage ?>&action=getinfos&file=<?php echo $row['nom'] . ' ' . $row['prenom'] . '.xml' ?>" name="fichier_xml" >Télécharger le fichier xml (version 1)</a>
							</li><br/>
							<li>
								<a href="export2.php?id=<?php echo $idpage ?>&action=getinfos&file=<?php echo $row['nom'] . ' ' . $row['prenom'] . '.xml' ?>" name="fichier_xml" >Télécharger le fichier xml (version 2)</a>
							</li><br/>
						</div>
					</div>
				</div>
			</div>
	  <?php
    }
?>
		<!-- Votre compte Informations personnelles-->

		<div class="container">
			<div class="row">
				<div class="col-lg-8 m-auto col-md-8 col-sm-12 col-xs-12">
					<div class="ui-block">
						<div class="ui-block-title">
							<h5 class="title">Infos persos</h5>
						</div>
						<div class="ui-block-content">
						
								<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<li>
									<span class="title">Nom : </span>
									<span class="text"><?php echo $row['nom']; ?></span>
								</li>
								
								<li>
									<span class="title">Prénom : </span>
									<span class="text"><?php echo $row['prenom']; ?></span>
								</li>
								
								<li>
									<span class="title">Description : </span>
									<span class="text"><?php echo $row['description']; ?></span>
								</li>
								
								</div>
								
							
							
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								
								
								<li>
									<span class="title">Sexe : </span>
									<span class="text"><?php echo $row['sexe']; ?></span>
								</li>
								
								<li>
									<span class="title">Anniversaire : </span>
									<span class="text"><?php echo $row['datetimepicker']; ?></span>
								</li>
								
								<li>
									<span class="title">Pays : </span>
									<span class="text"><?php echo $row['pays']; ?></span>
								</li>
								
								
							</div>
							</div>
							
								<br/><br/>
								<h6 class="title">Personnalité :</h6>
								
							
							<div class="ui-block-content">					
								
								
								<li>
									<span class="title">Statut : </span>
									<span class="text"><?php echo $row['statut']; ?></span>
								</li>
								<br/>
								
								<li>
									<span class="title">Musiques/Artistes : </span>
									<span class="text"><?php echo $row['musique']; ?></span>
								</li>
								<br/>
								<li>
									<span class="title">Séries/TV : </span>
									<span class="text"><?php echo $row['serie']; ?></span>
								</li>
								<br/>
								<li>
									<span class="title">Hobbies : </span>
									<span class="text"><?php echo $row['hobbies']; ?></span>
								</li>
								
							</ul>

						</div>
						
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
	<?php

}
?>
	<!-- ... fin votre compte Informations personnelles -->




	<script src="js/moment.min.js"></script>
	<script src="js/daterangepicker.min.js"></script>

	</body>
	</html>
