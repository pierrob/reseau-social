<!-- BONIJOL Pierre et LECOCQ Coraline -->
<title>Chat</title>
<!-- Fenêtre de chat, envoi/réception des messages entre utilisateurs -->
<?php
/*récupération de valeurs*/
if(isset($_GET['page']))
{
$lien = $_GET['page'];
}

if(isset($_POST['page']))
{
$lien = $_POST['page'];
}

if(isset($_POST['id']))
{
$idreturn = $_POST['id'];
}

/*appel du fichier header.php et de lien*/
require_once 'header.php';
require_once $lien;
  
  
  
if( isset( $_POST['message'] )) //si  'message' est définie et differente de NULL
{
	if(!empty($_POST['message'])) //si 'message' n'est pas vide
	{
		/*récupération de données*/
		$destinataire = htmlspecialchars($_POST['destinataire']);
		$time = time();
		$contenu = htmlspecialchars($_POST['message']);
  		$idauteur = $id_log;
		
		/*insertion des messages dans la base de données*/
		$query=mysqli_query($con,"INSERT INTO messages(mp_time,mp_texte,mp_expediteur,mp_receveur) VALUES('$time','$contenu','$idauteur','$destinataire')");
		$url = '/box.php?id='.$idreturn.'&page=' . $lien.'';
		echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';
	
	}
}

if(isset($_GET['id'])) //si  'id' est définie et differente de NULL
{
	$idpagechat = $_GET['id']; //récupération de l'id

	$infosidchat=mysqli_query($con,"SELECT * FROM membres WHERE id='$idpagechat'");
	$getinfoschat = $infosidchat->fetch_array(MYSQLI_ASSOC);	
	
?>

<!DOCTYPE html>
<html lang="en">
<div class="header-spacer"></div>

<script>
window.onload = function() {
    $('.popup-chat-responsive').toggleClass('open-chat');
};
$(document).ready(function(){
	$(".js-chat-open").on('click', function () {
		$('.popup-chat-responsive').toggleClass('open-chat');
		return false
	});
	});
</script>

<style>
.chat-m {
  float: right;
    padding: 13px;
      background-color: #7c5ac2;
	        color: #fff; 
    margin-top: -10px;
    border-radius: 10px;
    margin-bottom: -5px;
    font-size: 12px; }
	.author-log {
  float: right;
   }
	.chat-d {
  float: left;
    padding: 13px;
    background-color: #f0f4f9;
    margin-top: -10px;
    border-radius: 10px;
    margin-bottom: -5px;
    font-size: 12px; }
</style>


<!-- Fenêtre de chat -->

<div class="ui-block popup-chat popup-chat-responsive">
	<div class="ui-block-title">
		<span class="icon-status online"></span>
		<h6 class="title" >Chat</h6>
		<div class="more">
			<svg class="little-delete js-chat-open"><use xlink:href="icons/icons.svg#little-delete"></use></svg>
		</div>
	</div>
	
	<div class="mCustomScrollbar">
		<ul class="notification-list chat-message chat-message-field">
		
<?php 
/*check des messages entre deux utilisateurs*/
 $check_msg = mysqli_query($con,"SELECT mp_time,mp_texte,mp_expediteur,mp_receveur FROM messages WHERE mp_expediteur='$id_log' AND mp_receveur ='$idpagechat' OR mp_expediteur='$idpagechat' AND mp_receveur='$id_log' ORDER BY mp_time ASC");
 
 $nbmsgs= $check_msg->num_rows;
 
 $premier = 1;
 $ancienexp = 0;
 
 if ($nbmsgs != 0)
 {
	 
?>

<?php
	 
 for ($pm = 0 ; $pm < $nbmsgs ; ++$pm)
    {
		$msgrow = $check_msg->fetch_array(MYSQLI_ASSOC);
		$receveur = $msgrow['mp_receveur'];
		$expediteur = $msgrow['mp_expediteur'];
		   
		 if($ancienexp == $expediteur && $premier != 1 && $expediteur == $id_log)
		{
		?>
			 	
			<li>		
				<div class="notification-event">
				<span class="chat-m"><?php echo $msgrow['mp_texte'];?></span>
				</div>	
			 
			</li>	
			
		<?php
		}
		 else if ($ancienexp == $expediteur && $premier != 1 && $expediteur != $id_log)
		{
		?>
			 	
			<li>		
				<div class="notification-event">
				<span class="chat-d"><?php echo $msgrow['mp_texte'];?></span>
				</div>	 
			</li>			 
		<?php	 
		}
		else
		{
		?>
			<li><!-- envoi des messages selon l'utilisateur qui envoi, recepteur ou emetteur avec affichage de l'avatar, date + heure -->
				<?php 
				if( $expediteur == $id_log)
				{
				?>
				
				<div class="author-log">
					<img width="40" height="40" src="<? echo $getinfosperso['avatar']; ?>" alt="author" class="mCS_img_loaded">
					<?php 
					}
					else
					{
					?>
				
					<div class="author-thumb">
					<img width="40" height="40" src="<? echo $getinfoschat['avatar']; ?>" alt="author" class="mCS_img_loaded">
						
					<?php 	
					}
					?>
					
					</div>
					<div class="notification-event">
					<?php if($expediteur == $id_log)
					{
					?>
					
					<span class="chat-m"><?php echo $msgrow['mp_texte'];?></span>
					
					<?php 
					}
					else
					{
					?>
						<span class="chat-message-item"><?php echo $msgrow['mp_texte'];?></span>
					<?php
					}
					?>
					
					<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18"><?php echo date('Y-m-d H:i:s', $msgrow['mp_time'] + 21600); //heure fr ?></time></span>
					</div>
			</li>
			
			<?php
			$ancienexp = $expediteur;
			$premier = 0;

		}	
	}
	?>
	</ul>

<?php
}
?>

		
				</div>
<script>
    document.onkeydown=function(evt){
        var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
        if(keyCode == 13)
        {
            document.sendmsg.submit();
        }
    }
</script>

	<!--Pour envoyer le message -->
	<form method="POST" action="box.php" name ="sendmsg">

		<div class="form-group label-floating is-empty">
			<label class="control-label">Appuyer sur entrer pour poster...</label>
			<input type="hidden" name="destinataire" value="<?php echo $idpagechat; ?>" />
			<input type="hidden" name="id" value="<?php echo $idpagechat; ?>" />
			<input type="hidden" name="page" value="<?php echo basename($_SERVER['PHP_SELF'])?>" />
			<textarea class="form-control" name="message" placeholder=""></textarea>
			
		</div>

	</form>

  </div>

<?php

}

?>

</html>