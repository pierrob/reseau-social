<!-- BONIJOL Pierre et LECOCQ Coraline -->
<title>Edition du mdp</title>

<?php
//appel du fichier header.php
require_once 'header.php';


$query=mysqli_query($con,"SELECT * FROM membres WHERE email='$email'");
$row = $query->fetch_array(MYSQLI_ASSOC);	

$lemdp = $row['mdp'];

if (isset($_POST['submit_mdp']))  								    //si  'submit_mdp' est définie et differente de NULL
	{
		  $ancien_mdp = htmlspecialchars($_POST['vraimdp']);
		  if ($ancien_mdp == $lemdp)  								//si dans le champ ancien mdp le mdp est le même que celui enregistré dans la base de données
		  {
			$nouveau_mdp = htmlspecialchars($_POST['newmdp']);   	//récupération des valeurs entrés dans les champs de texte
		    $confirmer_mdp = htmlspecialchars($_POST['new2mdp']);
			
				if($nouveau_mdp == $confirmer_mdp)                  //si le champs nouveau mdp correspond à celui de confirmer mdp 
				{
					$query2=mysqli_query($con,"UPDATE membres SET mdp='$confirmer_mdp' WHERE email='$email'");
					$erreur="<div class='alert alert-success'>Mdp changé avec succés</div>";
				}else
				{
				$erreur="<div class='alert alert-danger'>Erreur,veuillez recopier correctement votre mot de passe dans le champs Confirmer mot de passe</div>";
				}
		  }else 
		  {
			   $erreur="<div class='alert alert-danger'>Erreur, ce mot de passe ne correspond pas au mot de passe actuel</div>";
		  }
	}

?>



<!DOCTYPE html>
<html lang="en">
<head>

	<style>
	.texteviolet{color: #4A089B;}
	</style>
	
	<!-- Styles pour les plugins -->
	<link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-select.css">
</head>
<body>





<!--En-tête principal : Votre compte -->

<div class="main-header">
	<div class="content-bg-wrap">
		<div class="content-bg bg-account"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 m-auto col-md-8 col-sm-12 col-xs-12">
				<div class="main-header-content"><br/><br/>
					<h1 class="texteviolet">Le tableau de bord de votre compte</h1>
					<p class="texteviolet">Bienvenue dans le tableau de bord de votre compte! Ici vous trouverez tout ce dont vous avez besoin pour changer vos
											informations sur votre profil (vos paramètres),changer votre mot de passe et bien plus encore!<br/> 
					</p>
					<br/><br/>
				</div>
			</div>
		</div>



<!-- ...fin de en-tête principal : Votre compte  -->


		<!-- Informations personnelles -->

		<div class="container">
			<div class="row">
			
			<!-- onglet sur la gauche "editer profil" -->
				<div class="col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12 responsive-display-none">
					<div class="ui-block">
						<div class="your-profile">
							<div class="ui-block-title ui-block-title-small">
								<h5 class="title">Votre profil</h5>
							</div>
							
							<div id="accordion" role="tablist" aria-multiselectable="true">
								<div class="card">
									<div class="card-header" role="tab" id="headingOne">
										<h6 class="mb-0">
											<a data-toggle="collapse" data-parent="#accordion" href="parametre.php" aria-expanded="true" aria-controls="collapseOne">
												Editer profil
												<svg class="dropdown-arrow-icon"><use xlink:href="icons/icons.svg#dropdown-arrow-icon"></use></svg>
											</a>
										</h6>
									</div>

									<div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
										<ul class="your-profile-menu">
											<li>
												<a href="parametres.php">Infos Perso</a>
											</li>
											
											<li>
												<a href="changermdp.php">Mot de Passe</a>
											</li>
											
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			
			
			
				<!-- Fenêtre "modifier mdp" -->
				<div class="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
					<div class="ui-block">
						<div class="ui-block-title">
							<h6 class="title">Modifier votre mot de passe</h6>
						</div>
						<div class="ui-block-content">
							<form method="POST" action="changermdp.php"> 
								<div class="row">
									<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">Ancien mot de passe</label>
											<input class="form-control" placeholder="" type="text" value="" name="vraimdp" >
										</div>
										
										<div class="form-group label-floating">
											<label class="control-label">Nouveau mot de passe</label>
											<input class="form-control" placeholder="" type="text" value="" name="newmdp" >
										</div>
										
										<div class="form-group label-floating">
											<label class="control-label">Confirmer mot de passe</label>
											<input class="form-control" placeholder="" type="text" value="" name="new2mdp" >
										</div>

										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<button type="submit" value="Sauvegarder" class="btn btn-primary btn-lg full-width" name="submit_mdp">Sauvegarder</button>
										</div>
										
										<br/>
										
										<?php 
										if( isset( $_POST['submit_mdp'] ) && $_POST['submit_mdp'] == 'Sauvegarder' )
										{
										echo $erreur; 
										}			
										?>
										
									</div>	
								</div>
							</form>
						</div>
					</div>
				</div>

				
			</div>
		</div>
	</div>
</div>

<!-- ... fin Informations personnelles -->




<!--Script de champ de saisie Datepicker-->
<script src="js/moment.min.js"></script>
<script src="js/daterangepicker.min.js"></script>

</body>
</html>