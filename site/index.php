<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!--Page d'accueil du site : enregistrement de compte/connexion -->
	<title>Bienvenu</title>
<?php 

setcookie('email', $email, time() + 3600, null, null, false, true);
setcookie('pays', 'France', time() + 3600, null, null, false, true);

 session_start();


?>
 
<head>

<!-- jQuery first, then Other JS. -->
<script src="js/jquery-3.2.0.min.js"></script>
<!-- Js effects for material design. + Tooltips -->
<script src="js/material.min.js"></script>
<!-- Helper scripts (Tabs, Equal height, Scrollbar, etc) -->
<script src="js/theme-plugins.js"></script>
<!-- Init functions -->
<script src="js/main.js"></script>


<!-- Script de champ de saisie Datepicker-->
<script src="js/moment.min.js"></script>
<script src="js/daterangepicker.min.js"></script>


	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Police principale -->
	<script src="js/webfontloader.min.js"></script>
<script>
//Lorsque la page est chargée, obtenez la valeur # actuelle et marquez là comme "active" si l'attribut href correspond.
$(document).ready(function() {
     
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        
        localStorage.setItem('lastTab', $(this).attr('href'));
    });

   
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }
});

		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
		</script>


<?php

 $con=mysqli_connect("sql313.unaux.com","unaux_21621064","s0t5nwpd","unaux_21621064_social");
 
		$error = $mdp = $nom = $prenom = $datetimepicker = $email = $sexe = "";
		$error2 = $emaillog = $mdplog = "";
 

  if( isset( $_POST['action'] ) && $_POST['action'] == 'signup' )
  {
    
    $mdp = htmlspecialchars($_POST['mdp']);	
	$nom = htmlspecialchars($_POST['nom']);	
	$prenom = htmlspecialchars($_POST['prenom']);
	$datetimepicker = htmlspecialchars($_POST['datetimepicker']);
	$email = htmlspecialchars($_POST['email']);
	$sexe = htmlspecialchars($_POST['sexe']);
	$secret = rand(0, 1000);
	
	
	
	//vérifications des données entrées lors d'une inscription
    if ($mdp == "" || $nom == "" || $prenom == "" || $datetimepicker == "" ||$email == "" ||$sexe == "" )  
      $error = "Remplissez tous les champs<br><br>";
    else
    {
      $result = mysqli_query($con,"SELECT * FROM membres WHERE email='$email'");

      if ($result->num_rows)
      {
      	 $error = "Cet email existe déjà<br><br>";
      	 $result='<div class="alert alert-danger">Erreur : compte deja existant</div>';
      }
       
      else
      {
        mysqli_query($con,"INSERT INTO membres(mdp,nom,prenom,datetimepicker,email,sexe,secret,actif) VALUES('$mdp', '$nom', '$prenom', '$datetimepicker', '$email', '$sexe','$secret','0')");
		
		$to = $email;
	$sujet = "Mail d'activation";
	$message = 'http://www.projetwebupjv.unaux.com/verification.php?email='.$email.'&secret='.$secret.' ';
	$mailHeaders = "From: Admin\r\n";
	$mail = mail($to, $sujet, $message , $mailHeaders);
	if($mail)
	{
		$result='<div class="alert alert-success">Compte créé</div>';
		
	}
	else
	{
		 $result='<div class="alert alert-danger">Erreur : mail</div>';
	}
	
      }
    }
  }
  if( isset( $_POST['action'] ) && $_POST['action'] == 'login' )
  {

    $emaillog = htmlspecialchars($_POST['emaillog']);
    $mdplog = htmlspecialchars($_POST['mdplog']);


    
    if ($emaillog == "" || $mdplog == "")
        $error2 = "Vous devez remplir tous les champs<br>";
    else
    {
      $result2 = mysqli_query($con,"SELECT email,mdp FROM membres
        WHERE email='$emaillog' AND mdp='$mdplog'");

      if ($result2->num_rows == 0)
      {
		$result2='<div class="alert alert-danger">erreur</div>';
      }
      else
      {
      	$nomlog = mysqli_query($con,"SELECT id,nom,prenom FROM membres WHERE email='$emaillog' AND mdp='$mdplog'");
      	
      	$row = mysqli_fetch_assoc($nomlog);
		$id = $row["id"];
		$_SESSION['idlog'] = $row["id"];
        $_SESSION['nomlog'] = $row["nom"];
        $_SESSION['prenomlog'] = $row["prenom"];
	    $_SESSION['emaillog'] = $emaillog;
		$result2='<div class="alert alert-success">Connecté! Redirection...</div>';

		header('Refresh: 3; URL=http://projetwebupjv.unaux.com/home.php?id='.$id.'');

      }
    }
  }

	 ?>
 
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.css">

	<!-- Theme Styles CSS -->
	<link rel="stylesheet" type="text/css" href="css/theme-styles.css">
	<link rel="stylesheet" type="text/css" href="css/blocks.css">
	<link rel="stylesheet" type="text/css" href="css/fonts.css">

	<!-- Styles for plugins -->
	<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-select.css">


</head>

<body class="landing-page">
<div class="content-bg-wrap">
	<div class="content-bg"></div>
</div>


<!-- En-tête d'atterrissage -->

<div class="container">
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12">
			<div id="site-header-landing" class="header-landing">
				<a href="" class="logo">
					<img src="img/logo.png" alt="Projet Web">
					<h5 class="logo-title">Projet Web</h5>
				</a>

			</div>
		</div>
	</div>
</div>

<!-- ... fin En-tête d'atterrissage -->

<!--Login-Formulaire d'inscription  -->

<div class="container">
	<div class="row display-flex">
		<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="landing-content">
				<h1>Bienvenu sur notre reseau social</h1>
				<p>Votre futur est arrivé, franchissez le pas, inscrivez-vous et entrez dans la nouvelle sphère du prochain leader des réseaux sociaux...<br/> MySocialNetwork !
				</p>
				
			</div>
		</div>

		<div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="registration-login-form">
				<!-- Onglets de navigation -->
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#home" role="tab">
							<svg class="login-icon"><use xlink:href="icons/icons.svg#login-icon"></use></svg>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" class="tab1" href="#profile" role="tab">
							<svg class="register-icon"><use xlink:href="icons/icons.svg#register-icon"></use></svg>
						</a>
					</li>
				</ul>

				<!-- Onglets -->
				<div class="tab-content">
					<div  class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab">
						<div class="title h6">Rejoindre le reseau</div>
						<form method='post' action='index.php' class="content">
							<div class="row">
								<div class="col-lg-6 col-md-6">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Nom</label>
										<input class="form-control" placeholder="" name="nom" value="" type="text">
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Prenom</label>
										<input class="form-control" placeholder="" name="prenom" value="" type="text">
									</div>
								</div>
								<div class="col-xl-12 col-lg-12 col-md-12">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Email</label>
										<input class="form-control" placeholder="" name="email" value="" type="email">
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Mot de passe</label>
										<input class="form-control" placeholder="" name="mdp" value="" type="password">
									</div>

									<div class="form-group date-time-picker label-floating">
										<label class="control-label">Anniversaire</label>
										<input name="datetimepicker" value="" />
										<span class="input-group-addon">
											<svg class="calendar-icon"><use xlink:href="icons/icons.svg#calendar-icon"></use></svg>
										</span>
									</div>

									<div class="form-group label-floating is-select">
										<label class="control-label">Genre</label>
										<select class="selectpicker form-control" name="sexe" size="auto">
											<option value="MA">Homme</option>
											<option value="FE">Femme</option>
										</select>
									</div>

									<div class="remember">
										<div class="checkbox">
											<label>
												<input name="optionsCheckboxes" type="checkbox">
												J'accepte <a href="#">les termes</a> du site
											</label>
										</div>
									</div>





									<button type="submit" name="action" value="signup" class="btn btn-purple btn-lg full-width">Finir l enregistrement!</button>

								</div>

								<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
	<?php	 if( isset( $_POST['action'] ) && $_POST['action'] == 'signup' ){
		 echo $result; 		}	 ?>
        </div>
    </div>

							</div>
						</form>
					</div>

					<div class="tab-pane" id="profile" role="tabpanel" data-mh="log-tab">
						<div class="title h6">Se connecter</div>
						<form method='post' action='index.php' class="content">
							<div class="row">
								<div class="col-xl-12 col-lg-12 col-md-12">
								
								 
									<div class="form-group label-floating is-empty">
										<label class="control-label">Email</label>
										<input class="form-control" placeholder="" type="email" name="emaillog" > 
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Mot de passe</label>
										<input class="form-control" placeholder=""  name="mdplog"  type="password"> 
									</div>

									<div class="remember">

										<div class="checkbox">
											<label>
												<input name="optionsCheckboxes" type="checkbox">
												Se souvenir
											</label>
										</div>
										<a href="#" class="forgot">Mot de passe oublié</a>


									

									<button type="submit" name="action" value="login" class="btn btn-lg btn-primary full-width" >Connexion</button>

						
									
																	<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
		<?php if( isset( $_POST['action'] ) && $_POST['action'] == 'login' ){
			
			
             echo $result2; 

		
			 }			?>
        </div>
    </div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</body>
</html>