<title>Anniversaires</title>

<?php
require_once 'header.php';

?>

<!DOCTYPE html>
<html lang="en">
<div class="header-spacer"></div>
	
	
	<div class="container">
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Janvier</h6>
				</div>
			</div>
		</div>
		
		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '01')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>
		

		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Février</h6>
				</div>
			</div>
		</div>
		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '02')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>

		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Mars</h6>
				</div>
			</div>
		</div>

		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '03')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>

		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Avril</h6>
				</div>
			</div>
		</div>

		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '04')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Mai</h6>
				</div>
			</div>
		</div>
		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '05')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Juin</h6>
				</div>
			</div>
		</div>
		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '06')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Juillet</h6>
				</div>
			</div>
		</div>
		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '07')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>

		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Aout</h6>
				</div>
			</div>
		</div>
		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '08')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Septembre</h6>
				</div>
			</div>
		</div>

		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '09')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>

		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Octobre</h6>
				</div>
			</div>
		</div>
		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '10')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Novembre</h6>
				</div>
			</div>
		</div>
		<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '11')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Decembre</h6>
				</div>
			</div>
		</div>

<?php
$queryhb = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,avatar FROM membres");

$numhb = $queryhb->num_rows;
for ($hb = 0;$hb < $numhb;++$hb)
{

    $rowhb = $queryhb->fetch_array(MYSQLI_ASSOC);

    $dateden = $rowhb['datetimepicker'];
    $mois = explode("/", $dateden);

    if ($mois[1] == '12')

    {

?>
		<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="birthday-item inline-items">
					<div class="author-thumb">
						<img width="42" height="42" src="<?php echo $rowhb['avatar']; ?>" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="home.php?id=<?php $rowhb['id'] ?>" class="h6 author-name"><?php echo $rowhb['nom'] . ' ' . $rowhb['prenom']; ?> </a>
						<div class="birthday-date"><?php echo $dateden; ?></div>
					</div>
					
				</div>
			</div>
		</div>
		<?php
    }
}
?>

	</div>
</div>
	
	
</body>
</html>
