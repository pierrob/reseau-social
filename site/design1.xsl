<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!-- Script xsl version 1 pour le fichier xml généré sur la page info à télécharger -->
	<head>
	<title> Informations </title>
	</head>
		<body> 
			<h2 align="center">Informations concernants <xsl:value-of select="informations/prenom"/> </h2>
			<table border="1" cellspacing="1" cellpadding="7" align="center">
				<tr bgcolor="#FEC3A6"> 	
					<td>Nom</td> 
					<td>Prenom</td> 
					<td>Sexe</td>
					<td>Anniversaire</td>
					<td>Pays</td> 
					<td>Statut</td>   
				</tr>
				<tr>
					<td><xsl:value-of select="informations/nom"/></td>
					<td><xsl:value-of select="informations/prenom"/></td> 
					<td><xsl:value-of select="informations/sexe"/></td> 
					<td><xsl:value-of select="informations/anniversaire"/></td> 
					<td><xsl:value-of select="informations/pays"/></td> 
					<td><xsl:value-of select="informations/statut"/></td>
				</tr> 
			</table>
			<br/>
			<table border="1" cellspacing="1" cellpadding="6" align="center">
				<tr bgcolor="#BCA0FE"> 	
					<td>Description</td> 
					<td>Musiques</td> 
					<td>Series</td>
					<td>Hobbies</td> 
				</tr>
				<tr>
					<td><xsl:value-of select="informations/description"/></td> 
					<td><xsl:value-of select="informations/musiques"/></td> 
					<td><xsl:value-of select="informations/series"/></td> 
					<td><xsl:value-of select="informations/hobbies"/></td>
				</tr> 
			</table>
		<div  style="margin-left:1100px; font-size:15pt">
		<br/><br/>A bientôt sur MySocialNetwork
		</div>
    
		</body> 
</html>
</xsl:template> 
</xsl:stylesheet>
