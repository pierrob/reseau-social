<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!--Page principale de l'utilisateur ! -->
<?php
require_once 'header.php';

if (isset($_GET['id']))
{
    $idpage = $_GET['id'];

    $infosid = mysqli_query($con, "SELECT * FROM membres WHERE id='$idpage'");
    $getinfos = $infosid->fetch_array(MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<div class="header-spacer"></div>
	<link rel="stylesheet" type="text/css" href="css/bootstrap-select.css">

<!-- En-tête supérieur -->

<div class="container">
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="top-header">
					<div class="top-header-thumb">
						<img src="img/header.jpg" alt="nature">
					</div>
					<div class="profile-section">
						<div class="row">
							<div class="col-lg-5 col-md-5 ">
								<ul class="profile-menu">
									<li>
										<a href="news.php" >Actualité</a>
									</li>
									
									<li>
										<a href="info.php?id=<?php echo $idpage ?>">Infos</a>
									</li>
									
									<li>
										<a href="amis.php?id=<?php echo $idpage ?>">Amis</a>
									</li>
									
								</ul>
							</div>
						</div>

						<div class="control-block-button">
							
<?php if ($id_log != $idpage)
	{
?>
						<div class="btn btn-control bg-primary more">
							<svg class="happy-face-icon"><use xlink:href="icons/icons.svg#happy-face-icon"></use></svg>
								<ul class="more-dropdown more-with-triangle triangle-bottom-right">
									<li>
		<?php
        $check_amis = mysqli_query($con, "SELECT id_1 FROM amis WHERE id_1='$id_log' AND id_2 ='$idpage' OR id_1='$idpage' AND id_2='$id_log'");

        $exist = $check_amis->num_rows;

        if ($exist == 1)
        {
		?>
			<a href="requetes.php?action=supprimer&id_emmetteur=<?php echo $id_log ?>&id_receveur=<?php echo $idpage ?>" name="action" value="requette"  >Supprimer ami</a>
		<?php
        }
        else
        {
		?>
			<a href="requetes.php?action=requete&id_emmetteur=<?php echo $id_log ?>&id_receveur=<?php echo $idpage ?>" name="action" value="requette"  >Ajouter en ami</a>
																			<?php
        }
?>
									</li>
								</ul>
						</div>
							
							<a href="#" class="btn btn-control bg-purple">
								<svg class="chat---messages-icon"><use xlink:href="icons/icons.svg#chat---messages-icon"></use></svg>
							</a>
<?php
    }
    else
    {
?>

							
						<div class="btn btn-control bg-primary more">
							<svg class="settings-icon"><use xlink:href="icons/icons.svg#settings-icon"></use></svg>
								<ul class="more-dropdown more-with-triangle triangle-bottom-right">
									<li>
										<a href="img.php" name="photo_profil" data-toggle="modal" data-target="#update-pdp">Mettre à jour photo de profil</a>
									</li>
									
									<li>
										<a href="img.php" data-toggle="modal" data-target="#update-header-photo">Mettre à jour couverture</a>
									</li>
									
									<li>
										<a href="parametres.php">Paramètres du compte</a>
									</li>
									
								</ul>
						</div>
<?php
    }
?>
							
						</div>
					</div>
					<!--affichage photo de profil + sous la photo de profil-->
					<div class="top-header-author">
						<a href="#" class="author-thumb">
							<img style='height: 100%; width: 100%; object-fit: contain' src="<?php echo $getinfos['avatar']; ?>" alt="author">
						</a>
						<div class="author-content">
							<a href="02-ProfilePage.html" class="h4 author-name"><?php echo $getinfos['nom'] . ' ' . $getinfos['prenom']; ?></a>
							<div class="country"><?php echo $getinfos['pays']; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ...fin En-tête supérieur -->


<!--Contenu principal -->
<div class="container">
	<div class="row">


		<!--Publications -->
		<div  class="col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-xs-12">
			<div id="newsfeed-items-grid">
				<div class="ui-block">
					<article class="hentry post">
						<form method="POST" action="img.php" enctype="multipart/form-data">  
						<input type="hidden" name="auteur" value="<?php echo $userstr; ?>" />
						<input type="hidden" name="auteurid" value="<?php echo $id_log; ?>" />
						<input type="hidden" name="idpage" value="<?php echo $idpage; ?>" />
						
						<textarea class="form-control" placeholder="Contenu de la publication" name="contenu" value=""></textarea>

						<!-- Confidentialité de la publication -->
						<div class="form-group label-floating is-select">
							<label class="control-label">Confidentialité</label>
								<select class="selectpicker form-control" name="confidentialite" size="auto">
									<option value="1">Publique</option>
									<option value="2">Amis</option>
									<option value="3">Privée</option>
								</select>
						</div>

						<input id="uploadpubli" name="uploadpubli" type="file"/>
						<li>
							<a href="#" id="upload_publi" name="upload_publi" >Ajouter une photo à la publication</a>
						</li><br/>
						<input type="submit"  name="action" value="poster" class="btn btn-purple btn-lg full-width" value="Poster la publication" />
						</form>								  
					</article>
				</div>
			</div>	

			<a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid"><svg class="three-dots-icon"><use xlink:href="icons/icons.svg#three-dots-icon"></use></svg></a>
		</div>
		
	<?php

    $query = mysqli_query($con, "SELECT * FROM publications WHERE id_page='$idpage' ORDER BY time DESC");
    $num = $query->num_rows;

    for ($j = 0;$j < $num;++$j)
    {

        $row = $query->fetch_array(MYSQLI_ASSOC);
        $publi = $row['id_publication'];
        $auteur = $row['id_auteur'];
        $confidentialite = $row['confiden'];
        $id_pageactu = $row['id_page'];

        $info_personne = mysqli_query($con, "SELECT * FROM membres WHERE id='$auteur'");

        $infospersonne = $info_personne->fetch_array(MYSQLI_ASSOC);

        $check_likes = mysqli_query($con, "SELECT id FROM likes WHERE id_publi='$publi' AND id_membre ='$id_log'");
        $listlike = mysqli_query($con, "SELECT id_membre FROM likes WHERE id_publi='$publi'");
        $listdislike = mysqli_query($con, "SELECT id_membre FROM dislikes WHERE id_publi='$publi'");

        $check_dislikes = mysqli_query($con, "SELECT id FROM dislikes WHERE id_publi='$publi' AND id_membre ='$id_log'");
        $bool_like = $check_likes->num_rows;
        $bool_dislike = $check_dislikes->num_rows;

        $likes = mysqli_query($con, "SELECT id FROM likes WHERE id_publi='$publi'");
        $likes = $likes->num_rows;

        $dislikes = mysqli_query($con, "SELECT id FROM dislikes WHERE id_publi='$publi'");
        $dislikes = $dislikes->num_rows;

        $estamis = mysqli_query($con, "SELECT * FROM amis WHERE id_1='$idpage' AND id_2='$id_log' OR id_1='$id_log' AND id_2='$idpage'");
        $estamisnum = $estamis->num_rows;

        if ($confidentialite == '1' || $confidentialite == '2' && $estamisnum == 1 || $idpage == $id_log)
        {

?>

	  	  <!--Like / dislikes -->
 <div class="modal fade" id="modal<?php echo $j; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Likes</h4>
            </div>
            <div class="modal-body">
				<ul class="list-group">
<?php
            while ($mem = mysqli_fetch_assoc($listlike)):
                $idlist = $mem['id_membre'];
                $listquery = mysqli_query($con, "SELECT nom,prenom,id FROM membres WHERE id='$idlist'");
                $listnom = $listquery->fetch_array(MYSQLI_ASSOC);
                echo '<a href="/home.php?id=' . $listnom['id'] . '"><li class="list-group-item">' . $listnom['nom'] . ' ' . $listnom['prenom'] . '</li></a>';
            endwhile;
?>
				</ul>
            </div>
		</div>
    </div>
</div>

	<div class="modal fade" id="modald<?php echo $j; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Dislikes</h4>
                </div>
                <div class="modal-body">
					<ul class="list-group">
<?php
            while ($mem = mysqli_fetch_assoc($listdislike)):
                $idlistd = $mem['id_membre'];

                $listqueryd = mysqli_query($con, "SELECT nom,prenom,id FROM membres WHERE id='$idlistd'");
                $listnomd = $listqueryd->fetch_array(MYSQLI_ASSOC);
                echo '<a href="/home.php?id=' . $listnomd['id'] . '"><li class="list-group-item">' . $listnomd['nom'] . ' ' . $listnomd['prenom'] . '</li></a>';
            endwhile;
?>
					</ul>
                </div>
            </div>
        </div>
    </div>
				  
	<!--Affichage publication + modifier ou spprimer nos propres publications -->
	<div class="col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<article class="hentry post">
					<div class="post__author author vcard inline-items">
							<img width="42" height="42" src="<?php echo $infospersonne['avatar']; ?>" alt="author">
							<div class="author-date">
								<a class="h6 post__author-name fn" href="02-ProfilePage.html"><?php echo $row['auteur']; ?></a>
								<div class="post__date">
									<time class="published" datetime="2017-03-24T18:18">
									<?php echo date('Y-m-d H:i:s', $row['time'] + 21600); //heure fr?>
									</time>
								</div>
							</div>

							<?php if ($row['id_auteur'] == $id_log)
								{
							?>
							<div class="more"><svg class="three-dots-icon"><use xlink:href="icons/icons.svg#three-dots-icon"></use></svg>
								<ul class="more-dropdown">
									<li>
										<a href="modifier.php?contenu=<?php echo $row['contenu']; ?>&idpubli=<?php echo $row['id_publication']; ?>&auteurid=<?php echo $row['id_auteur']; ?>&monid=<?php echo $id_log; ?>">Modifier la publication </a>
									</li>
									
									<li>
										<a href="publications.php?action=delete&idpubli=<?php echo $row['id_publication']; ?>&auteurid=<?php echo $row['id_auteur']; ?>&monid=<?php echo $id_log; ?>" name="action" value="delete">Supprimer la publication</a>
									</li>
									
								</ul>
							</div>
							<?php
								}
							?>

					</div>

					<p> <?php echo $row['contenu']; ?> </p>
					
					<?php if (!empty($row['image']))
						{
					?>	
					
					<ul class="widget w-last-photo js-zoom-gallery">
					<a href="<?php echo $row['image']; ?>">
								<img style='height: 50%; width: 50%; object-fit: contain' src="<?php echo $row['image']; ?>">
							</a>
						</ul>
						<br/>
					
						
						<?php
						}
						?>

					<div class="post-additional-info inline-items">
						<a href="aimer.php?t=1&id_perso=<?php echo $id_log; ?>&id=<?php echo $row['id_publication']; ?>"> 
		     <?php if ($bool_like == 1)
            {
                echo "Je n'aime plus";
            }
            else
            {
                echo "J'aime";
            } ?></a><a  href="#modal<?php echo $j; ?>" data-toggle="modal" data-target="#modal<?php echo $j; ?>"><?php echo $likes ?></a>
					<a href="aimer.php?t=2&id_perso=<?php echo $id_log; ?>&id=<?php echo $row['id_publication']; ?>"> <?php if ($bool_dislike == 1)
            {
                echo "Retirer dislike";
            }
            else
            {
                echo "Je n'aime pas";
            } ?></a>
			 <a  href="#modald<?php echo $j; ?>" data-toggle="modal" data-target="#modald<?php echo $j; ?>"><?php echo $dislikes ?></a>
							
<?php
            $idpubli = $row['id_publication'];
            $query2 = mysqli_query($con, "SELECT * FROM commentaires WHERE id_publi_com='$idpubli'");

            $num2 = $query2->num_rows;
?>
							
						<div class="comments-shared">
							<a href="#" class="post-add-icon inline-items">
								<svg class="speech-balloon-icon"><use xlink:href="icons/icons.svg#speech-balloon-icon"></use></svg>
								<span><?php echo $num2; ?></span>
							</a>	
						</div>

					</div>
				</article>
				<ul class="comments-list">
<?php
            for ($h = 0;$h < $num2;++$h)
            {
                $row2 = $query2->fetch_array(MYSQLI_ASSOC);
                $publi2 = $row2['id_commentaire'];
                $idauter = $row2['id_auteur'];
                $avatarquery = mysqli_query($con, "SELECT avatar FROM membres WHERE id='$idauter'");
                $avatar = $avatarquery->fetch_array(MYSQLI_ASSOC);
?>
					<!--affichage commentaires -->
					<li>
						<div class="post__author author vcard inline-items">
							<img src="<?php echo $avatar['avatar']; ?>" alt="author">
								<div class="author-date">
									<a class="h6 post__author-name fn" href="#"><?php echo $row2['auteur']; ?></a>
										<div class="post__date">
											<time class="published" datetime="2017-03-24T18:18">
											<?php echo date('Y-m-d H:i:s', $row2['time'] + 21600); //heure fr ?>
											</time>
										</div>
								</div>
						</div>
						<p> <?php echo $row2['commentaire']; ?> </p>
					</li>							
<?php
            }
?>
	<!-- Commentaires -->
	<form method="POST" action="commentaire.php">
		<input type="hidden" name="auteur" value="<?php echo $userstr; ?>" />
		<input type="hidden" name="auteurid" value="<?php echo $id_log; ?>" />
		<input type="hidden" name="idpubli" value="<?php echo $row['id_publication']; ?>" />
		<textarea class="form-control" name="commentaire" placeholder="Exprimez-vous..." value="" ></textarea><br/>
		<input type="submit" value="Poster votre commentaire" class="btn btn-primary" name="submit_commentaire" />
	</form>
						
		</div> 	
	</div>
	
<?php
        }

    }
?>

		<!--Barre latérale de gauche -->
		<!--affichage de quelques infos sur l'utilisateur, à retrouver à la page info-->
		<div class="col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Profile Intro</h6>
				</div>
				<div class="ui-block-content">
					<ul class="widget w-personal-info item-block">
						<li>
							<span class="title">A propos de moi</span>
							<span class="text"><?php echo $getinfos['description']; ?></span>
						</li>
						
						<li>
							<span class="title">Musiques/Artistes</span>
							<span class="text"><?php echo $getinfos['musique']; ?></span>
						</li>
						
						<li>
							<span class="title">Séries/TV</span>
							<span class="text"><?php echo $getinfos['serie']; ?></span>
						</li>
						
						<li>
							<span class="title">Hobbies:</span>
							<span class="text"><?php echo $getinfos['hobbies']; ?></span>
						</li>
						
					</ul>


				</div>
			</div>
		</div>

		<!-- ... Fin de la barre latérale gauche -->
</div>

 
<style type="text/css">
    #upload{
    display: none;
}
    #uploadpubli{
    display: none;
}
</style>

<!-- Fenêtre-popup Mettre à jour l'en-tête pdp -->
<!--Changer la photo de profil -->
<form method="POST" action="img.php" enctype="multipart/form-data">  
	<div class="modal fade" id="update-pdp">
		<div class="modal-dialog ui-block window-popup update-header-photo">
			<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="close-icon"><use xlink:href="icons/icons.svg#close-icon"></use></svg>
			</a>
			<input type="hidden" name="auteurid" value="<?php echo $id_log; ?>" />
				<div class="ui-block-title">
				<h6 class="title">Mettre à jour la photo de profil</h6>
				<input type="submit" class="btn btn-green btn-lg full-width" name="submit" value="Mettre a jour" />
				</div>
			<input id="upload" name="upload" type="file"/>
			<a class="upload-photo-item" id="upload_link">
				<svg class="computer-icon"><use xlink:href="icons/icons.svg#computer-icon"></use></svg>

			<h6>Envoyer la photo</h6>
			<span>Parcourez votre ordinateur.</span>
			</a>

			<a href="#" class="upload-photo-item" data-toggle="modal" data-target="#choose-from-my-photo">

			<svg class="photos-icon"><use xlink:href="icons/icons.svg#photos-icon"></use></svg>

			<h6>Choisir parmi mes photos</h6>
			<span>Choisissez parmi vos photos téléchargées</span>
			</a>
		</div>
	</div>
</form>



<!-- ... Fin Fenêtre-popup Mettre à jour l'en-tête photo -->


</div>
<?php

}
?>


<script>
 $(document).ready(function(){
    $("#upload_link").on('click', function(e){
        e.preventDefault();
        $("#upload:hidden").trigger('click');
    });
});

 $(document).ready(function(){
    $("#upload_publi").on('click', function(e){
        e.preventDefault();
        $("#uploadpubli:hidden").trigger('click');
    });
});
</script>

</body>
</html>
