<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!-- Script xsl version 2 pour le fichier xml généré sur la page info à télécharger -->
	<head>
	<title> Informations </title>
	</head>
		<body style="font-family:Arial; font-size:12pt;"> 
			<div style="background-color:#F55D26; color:white;"> 
				<span style="font-weight:bold; color:white; padding:4px">  
				<h2 align="center">Informations concernants <xsl:value-of select="informations/prenom"/> </h2>
				</span>
			</div > 
			<div  style="margin-left:420px; font-size:18pt">
				<span > <br/>Nom : <xsl:value-of select="informations/nom"/> <br/> Prénom : <xsl:value-of select="informations/prenom"/> <br/>
				</span ><br/>
			</div>
			<div  style="margin-left:420px; font-size:16pt">
				<span style="font-style:italic"> -Sexe :  <xsl:value-of select="informations/sexe"/> <br/>
				-Anniversaire : <xsl:value-of select="informations/anniversaire"/> <br/>
				-Pays : <xsl:value-of select="informations/pays"/> <br/>
				-Statut : <xsl:value-of select="informations/statut"/> <br/>
				</span > <br/>
			</div>
			<div  style="margin-left:420px; font-size:15pt">
				<span > Description : <xsl:value-of select="informations/description"/> <br/> 
				Musiques : <xsl:value-of select="informations/musiques"/> <br/>
				Séries : <xsl:value-of select="informations/series"/> <br/>
				Hobbies : <xsl:value-of select="informations/hobbies"/>
				</span >
			</div>
			<div  style="margin-left:1100px; font-size:15pt">
			<br/><br/>A bientôt sur MySocialNetwork
			</div>
    
		</body> 
</html>
</xsl:template> 
</xsl:stylesheet>