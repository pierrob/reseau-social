<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!-- fenêtre de déconnexion amenant sur la page d'accueil -->
<?php
 session_start();
 
 // Supression des variables de session et de la session
 $_SESSION = array();
 session_destroy();
 header('Location: /index.php');        
 
?>