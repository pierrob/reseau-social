<!-- BONIJOL Pierre et LECOCQ Coraline -->
<title>Amis</title>
<!-- Page d'affichage des amis -->
<?php

  require_once 'header.php';
  if(isset($_GET['id']))
	{
	$idpage2 = $_GET['id'];

	/*pour avoir le nombre d'amis*/
	$infosamis=mysqli_query($con,"SELECT * FROM amis WHERE id_1='$idpage2' OR id_2='$idpage2'");
	$nombreamis = $infosamis->num_rows;
 

?>

<!DOCTYPE html>
<html lang="en">
<head>

	<style>
	.texteviolet{color: #4A089B;}
	</style>


</head>
<body>
    <!-- Description du haut de page --> 
	<div class="header-spacer header-spacer-small"></div>
		<div class="main-header">
			<div class="content-bg-wrap">
				<div class="content-bg bg-group"></div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 m-auto col-md-8 col-sm-12 col-xs-12">
						<div class="main-header-content">
							<h1  class="texteviolet">Amis</h1>
							<p  class="texteviolet">Bienvenue sur votre page d'amis ! <br/>
													Ici vous pourrez visualiser rapidement la liste de vos amis, et aller rapidement sur leur profil !   		
													
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
					
		
		
		
<!-- Faire la liste d'amis et l'afficher -->

	<div class="container">
		<div class="row">


			<?php 
			
			for ($x = 0 ; $x < $nombreamis ; ++$x)
			{
				$infosa = $infosamis->fetch_array(MYSQLI_ASSOC);

				$id_1 = $infosa['id_1'];
				$id_2 = $infosa['id_2'];

				if($id_1 == $idpage2)	
				{
					$infoamis2 = mysqli_query($con,"SELECT * FROM membres WHERE id='$id_2'");

				}
				else
				{
					$infoamis2 = mysqli_query($con,"SELECT * FROM membres WHERE id='$id_1'");
				}

			$infofinal = $infoamis2->fetch_array(MYSQLI_ASSOC);
			?>



			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<div class="ui-block" data-mh="friend-groups-item">
					<div class="friend-item friend-groups">

						<div class="friend-item-content">

							<div class="more">
								<svg class="three-dots-icon"><use xlink:href="icons/icons.svg#three-dots-icon"></use></svg>
								<ul class="more-dropdown">
									<?php if ($idpage2 == $id_log)
									{
									?>
									<!--On peut supprimer l'amis seulement si ce sont nos amis personnels (impossible de supprimer les amis des autres)-->
									<li>
										<a href="requetes.php?action=supprimer&id_emmetteur=<?php echo $id_log?>&id_receveur=<?php echo $infofinal['id']?>">Supprimer des amis</a>
									</li>
									
									<?php
									}
									?>	
									
									<!-- on peut envoyer un message à la personne, qu'elle soit notre amis ou non -->
									<li>
										<a href="box.php?id=<?php echo $infofinal['id']?>&page=<?php echo basename($_SERVER['PHP_SELF'])?>">Envoyer un message</a>
									</li>
								</ul>
							</div>
							<div class="friend-avatar">
								<div class="author-thumb">
									<img width="124" height="124" src="<?php echo $infofinal['avatar']; ?>" >
								</div>
								<div class="author-content">
									<a href="/home.php?id=<?php echo $infofinal['id'] ?>" class="h5 author-name"><?php echo $infofinal['nom'] . ' ' . $infofinal['prenom']; ?></a>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>

		<?php 
		}
		?>
		</div>
	</div>

<?php
}
?>
</body>
</html>