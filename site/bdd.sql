-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Client: sql313.unaux.com
-- Généré le: Dim 06 Mai 2018 à 11:00
-- Version du serveur: 5.6.35-81.0
-- Version de PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `unaux_21621064_social`
--

-- --------------------------------------------------------

--
-- Structure de la table `amis`
--

CREATE TABLE IF NOT EXISTS `amis` (
  `id_1` int(3) DEFAULT NULL,
  `id_2` int(3) DEFAULT NULL,
  KEY `utilisateur` (`id_1`),
  KEY `ami` (`id_2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `amis`
--

INSERT INTO `amis` (`id_1`, `id_2`) VALUES
(2, 1),
(1, 16),
(4, 2),
(18, 1),
(19, 1),
(21, 20),
(21, 22),
(22, 20);

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE IF NOT EXISTS `commentaires` (
  `id_commentaire` int(11) NOT NULL AUTO_INCREMENT,
  `auteur` varchar(250) NOT NULL,
  `commentaire` text NOT NULL,
  `id_auteur` int(11) NOT NULL,
  `id_publi_com` int(11) NOT NULL,
  `time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_commentaire`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Contenu de la table `commentaires`
--

INSERT INTO `commentaires` (`id_commentaire`, `auteur`, `commentaire`, `id_auteur`, `id_publi_com`, `time`) VALUES
(24, 'Pruvot Coraline', 'youyou', 1, 51, 1525129581),
(25, 'Pruvot Coraline', 'yeye', 1, 51, 1525129591),
(26, 'Lecocq Lecocq', 'klkl', 4, 53, 1525186188),
(27, 'Pruvot Coraline', '5454', 1, 51, 1525204925),
(28, 'Pruvot Coraline', '656565\r\n', 1, 51, 1525204931),
(29, 'Pruvot Coraline', '5565\r\n', 1, 51, 1525204986),
(30, 'Jkl Jkljkl', 'Com 1', 2, 33, 1525265346),
(31, 'Jkl Jkljkl', 'Com 2', 2, 33, 1525265353),
(32, 'Pruvot Maxence', 'jolie', 18, 51, 1525276147),
(33, 'Pruvot Coraline', '545\r\n', 1, 68, 1525387625),
(34, 'Pruvot Coraline', 'jadore', 1, 69, 1525436753),
(35, 'Lecocq Coraline', 'test test', 20, 83, 1525608405),
(36, 'Pruvot Maxence', 'belles marmottes', 21, 85, 1525608599),
(37, 'Lecocq Coraline', 'superbe\r\n', 20, 87, 1525608832);

-- --------------------------------------------------------

--
-- Structure de la table `dislikes`
--

CREATE TABLE IF NOT EXISTS `dislikes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_publi` int(11) NOT NULL,
  `id_membre` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Contenu de la table `dislikes`
--

INSERT INTO `dislikes` (`id`, `id_publi`, `id_membre`) VALUES
(62, 84, 21);

-- --------------------------------------------------------

--
-- Structure de la table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_publi` int(11) NOT NULL,
  `id_membre` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=117 ;

--
-- Contenu de la table `likes`
--

INSERT INTO `likes` (`id`, `id_publi`, `id_membre`) VALUES
(108, 82, 20),
(116, 88, 20),
(115, 87, 20),
(114, 86, 20),
(113, 83, 21),
(112, 85, 21),
(111, 85, 20),
(110, 83, 20),
(109, 84, 20);

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `mdp` varchar(16) DEFAULT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `prenom` varchar(30) DEFAULT NULL,
  `datetimepicker` varchar(20) DEFAULT NULL,
  `email` varchar(25) NOT NULL,
  `sexe` char(3) NOT NULL,
  `secret` int(4) NOT NULL,
  `actif` int(1) NOT NULL,
  `tel` int(10) DEFAULT NULL,
  `description` varchar(50000) DEFAULT NULL,
  `ldn` varchar(40) DEFAULT NULL,
  `hobbies` varchar(400) DEFAULT NULL,
  `pays` varchar(20) DEFAULT NULL,
  `musique` varchar(500) DEFAULT NULL,
  `serie` varchar(500) DEFAULT NULL,
  `avatar` varchar(200) NOT NULL,
  `statut` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Contenu de la table `membres`
--

INSERT INTO `membres` (`id`, `mdp`, `nom`, `prenom`, `datetimepicker`, `email`, `sexe`, `secret`, `actif`, `tel`, `description`, `ldn`, `hobbies`, `pays`, `musique`, `serie`, `avatar`, `statut`) VALUES
(20, '123', 'Lecocq', 'Coraline', '02/03/1997', 'lecocq.coraline@live.fr', 'FE', 4, 0, 633004337, 'Etudiante en L2 informatique', 'nord', 'sport ', 'France', 'Ariana grande \r\nPop musique', 'TWD\r\nGame of thrones\r\nPretty Little Liars', 'img/kkk.jpg', 'good'),
(21, 'max14', 'Pruvot', 'Maxence', '14/09/1997', 'max14@gmail.com', 'MA', 208, 0, 0, 'la mozza la vie', 'amiens', 'sport', 'France', 'tout', 'shameless \r\ngot \r\ntwd', 'img/IMG_1062.JPG', NULL),
(22, 'saraha', 'Aubert', 'Sarah', '19/04/1997', 'sarahA@live.fr', 'FE', 699, 0, 0, 'yeah le soleil', 'australie', 'velo', 'Australia', 'tout', 'dr House', 'img/IMG_9767.JPG', NULL),
(23, 'tzst', 'Bonijol', 'Pierre', '09/01/2018', 'test@gmail.com', 'MA', 259, 0, 694848, 'Test', 'Dd', 'Vgff', 'France', 'Test', 'Dd', '', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `mp_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mp_time` int(11) unsigned NOT NULL,
  `mp_texte` varchar(50000) NOT NULL,
  `mp_expediteur` int(11) NOT NULL,
  `mp_receveur` int(11) NOT NULL,
  PRIMARY KEY (`mp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Contenu de la table `messages`
--

INSERT INTO `messages` (`mp_id`, `mp_time`, `mp_texte`, `mp_expediteur`, `mp_receveur`) VALUES
(41, 1525608915, 'Ã§a va et toi ?', 20, 21),
(40, 1525608880, 'tu vas bien ? ', 21, 20),
(39, 1525608869, 'salut ', 21, 22),
(38, 1525608807, 'coucou', 20, 21),
(37, 1525608744, 'salut', 22, 21);

-- --------------------------------------------------------

--
-- Structure de la table `publications`
--

CREATE TABLE IF NOT EXISTS `publications` (
  `id_publication` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `id_page` int(3) NOT NULL,
  `auteur` varchar(30) NOT NULL,
  `contenu` text NOT NULL,
  `time` int(10) unsigned NOT NULL,
  `image` varchar(100) NOT NULL,
  `confiden` int(1) NOT NULL,
  PRIMARY KEY (`id_publication`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

--
-- Contenu de la table `publications`
--

INSERT INTO `publications` (`id_publication`, `id_auteur`, `id_page`, `auteur`, `contenu`, `time`, `image`, `confiden`) VALUES
(83, 23, 23, 'Bonijol Pierre', 'Test', 1525607966, '', 1),
(88, 22, 22, 'Aubert Sarah', 'il fait beau ', 1525608642, '', 1),
(87, 21, 21, 'Pruvot Maxence', 'salut Ã  tous', 1525608554, 'img/GOPR0135.JPG', 2),
(86, 21, 21, 'Pruvot Maxence', 'hello', 1525608491, '', 1),
(85, 20, 20, 'Lecocq Coraline', 'marmottes', 1525608425, 'img/marmotte.jpg', 1),
(84, 20, 20, 'Lecocq Coraline', 'mes amis peuvent voir cette publication mais pas les autres', 1525608191, '', 2),
(82, 20, 20, 'Lecocq Coraline', 'salut', 1525607679, '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `requete`
--

CREATE TABLE IF NOT EXISTS `requete` (
  `id_requette` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(3) NOT NULL,
  `id_receveur` int(3) NOT NULL,
  `accepter` int(1) NOT NULL,
  PRIMARY KEY (`id_requette`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `requete`
--

INSERT INTO `requete` (`id_requette`, `id_auteur`, `id_receveur`, `accepter`) VALUES
(7, 1, 16, 1),
(4, 2, 4, 0),
(8, 18, 1, 1),
(9, 19, 1, 1),
(10, 21, 20, 1),
(11, 21, 23, 0),
(12, 20, 23, 0),
(13, 21, 22, 1),
(14, 22, 20, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
