<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!--Page pour modifier une publication après l'avoir posté -->
<head>
<title>Paramètres du compte</title>
</head>
<?php
require_once 'header.php';

$query = mysqli_query($con, "SELECT * FROM membres WHERE email='$email'");
$row = $query->fetch_array(MYSQLI_ASSOC);

if (isset($_POST['auteurid']))
{
    if (!empty($_POST['auteurid']))
    {

        $contenu = htmlspecialchars($_POST['contenu']);
        $idauteur = htmlspecialchars($_POST['auteurid']);
        $idpubli = htmlspecialchars($_POST['idpubli']);
        $monid = htmlspecialchars($_POST['monid']);

        if ($monid == $idauteur)
        {
            $query = mysqli_query($con, "update publications set contenu='$contenu' WHERE id_publication='$idpubli' AND id_auteur ='$idauteur'");
        }

        $url = '/home.php?id=' . $idauteur . '';
        echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL=' . $url . '">';

    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	
</head>
<body>

</br></br></br></br></br></br>

<div class="container">
	<div class="row">


		<div  class="col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-xs-12">
			<div id="newsfeed-items-grid">
				<div class="ui-block">
					<article class="hentry post">
						<form method="POST" action="modifier.php">  
							<input type="hidden" name="auteurid" value="<?php echo $_GET['auteurid']; ?>" />
							<input type="hidden" name="idpubli" value="<?php echo $_GET['idpubli']; ?>" />
							<input type="hidden" name="monid" value="<?php echo $_GET['monid']; ?>" />
							<textarea class="form-control" placeholder="" name="contenu" value=""><?php echo $_GET['contenu']; ?></textarea>
							<input type="submit"  name="action" value="modifier" class="btn btn-purple btn-lg full-width" value="Modifier la publication" />
						</form>		

					</article>
				</div>
			</div>	
		</div>		
	</div>	
</div>			

</body>
</html>
