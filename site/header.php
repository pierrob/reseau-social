<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!-- fichier header, la tête du site : donc sidebar gauche, sidebar droite + sidebar du haut-->
<?php
session_start();

$userstr = 'Invité';

if (isset($_SESSION['nomlog']))
{
    $nom = $_SESSION['nomlog'];
    $prenom = $_SESSION['prenomlog'];
    $id_log = $_SESSION['idlog'];
    $email = $_SESSION['emaillog'];
    $loggedin = true;
    $userstr = ucfirst($nom) . ' ' . ucfirst($prenom);
}
else $loggedin = false;

if (!$loggedin) die();

if (isset($_GET['statut']))
{
    header('Location: /home.php?id=' . $id_log . '');
}

$con = mysqli_connect("sql313.unaux.com", "unaux_21621064", "s0t5nwpd", "unaux_21621064_social");

$infosperso = mysqli_query($con, "SELECT * FROM membres WHERE id='$id_log'");
$getinfosperso = $infosperso->fetch_array(MYSQLI_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
<head>

	<title>Page du Profil</title>


	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	


	<!-- police principale -->
    <script src="js/webfontloader.min.js"></script>

    <script>
	
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.css">

	<!-- Theme Styles CSS -->
	<link rel="stylesheet" type="text/css" href="css/theme-styles.css">
	<link rel="stylesheet" type="text/css" href="css/blocks.css">
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	
	
	<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css">
		<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">

</head>
<body>



<!-- Barre latérale fixe gauche -->

<div class="fixed-sidebar">
	<div class="fixed-sidebar-left sidebar--small" id="sidebar-left">
		<a href="home.php?id=<?php echo $id_log; ?>" class="logo">
			<img src="img/logo.png" alt="MySocialNetwork">
		</a>

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="left-menu">
				<li>
					<a href="#" class="js-sidebar-open">
						<svg class="menu-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="OUVRIR MENU"><use xlink:href="icons/icons.svg#menu-icon"></use></svg>
					</a>
				</li>
				<li>
					<a href="news.php">
						<svg class="newsfeed-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="ACTUALITE"><use xlink:href="icons/icons.svg#newsfeed-icon"></use></svg>
					</a>
				</li>
				<li>
					<a href="amis.php?id=<?php echo $id_log ?>">
						<svg class="happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="AMIS"><use xlink:href="icons/icons.svg#happy-faces-icon"></use></svg>
					</a>
				</li>
				<li>
					<a href="membres.php">
						<svg class="happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="MEMBRES DU RESEAU"><use xlink:href="icons/icons.svg#happy-faces-icon"></use></svg>
					</a>
				</li>
			
				<li>
					<a href="anniversaires.php">
						<svg class="cupcake-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="ANNIVERSAIRES"><use xlink:href="icons/icons.svg#cupcake-icon"></use></svg>
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1">
		<a href="home.php?id=<?php echo $id_log; ?>" class="logo">
			<img src="img/logo.png" >
			<h6 class="logo-title">MySocialNetwork</h6>
		</a>

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="left-menu">
				<li>
					<a href="#" class="js-sidebar-open">
						<svg class="close-icon left-menu-icon"><use xlink:href="icons/icons.svg#close-icon"></use></svg>
						<span class="left-menu-title">Fermer Menu</span>
					</a>
				</li>
				<li>
					<a href="news.php">
						<svg class="newsfeed-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="NEWSFEED"><use xlink:href="icons/icons.svg#newsfeed-icon"></use></svg>
						<span class="left-menu-title">Actualité</span>
					</a>
				</li>
			
				<li>
					<a href="amis.php">
						<svg class="happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="AMIS"><use xlink:href="icons/icons.svg#happy-faces-icon"></use></svg>
						<span class="left-menu-title">Amis</span>
					</a>
				</li>
				
				<li>
					<a href="membres.php">
						<svg class="happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="MEMBRES DU RESEAU"><use xlink:href="icons/icons.svg#happy-faces-icon"></use></svg>
						<span class="left-menu-title">Membres du réseau</span>
					</a>
				</li>
				
					<a href="anniversaires.php">
						<svg class="cupcake-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Friends Birthdays"><use xlink:href="icons/icons.svg#cupcake-icon"></use></svg>
						<span class="left-menu-title">Anniversaires</span>
					</a>
				</li>
			</ul>

		</div>
	</div>
</div>

<!-- ... fin barre latérale fixe gauche -->



<!-- Barre latérale fixe droite -->

<div class="fixed-sidebar right">
	<div class="fixed-sidebar-right sidebar--small" id="sidebar-right">

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="chat-users">
				
<?php
/*apparition des amis dans la sidebar droite */
$amischat = mysqli_query($con, "SELECT * FROM amis WHERE id_2='$id_log' OR id_1='$id_log'");
$numsami = $amischat->num_rows;

for ($f = 0;$f < $numsami;++$f)
{
    $sontamis = $amischat->fetch_array(MYSQLI_ASSOC);
    $id_1amis = $sontamis['id_1'];
    $id_2amis = $sontamis['id_2'];

    if ($sontamis['id_2'] == $id_log)
    {
        $infosamis = mysqli_query($con, "SELECT * FROM membres WHERE id='$id_1amis'");
        $informationssamis = $infosamis->fetch_array(MYSQLI_ASSOC);
    }
    else
    {
        $infosamis = mysqli_query($con, "SELECT * FROM membres WHERE id='$id_2amis'");
        $informationssamis = $infosamis->fetch_array(MYSQLI_ASSOC);
    }
?>
			<a href="box.php?id=<?php echo $informationssamis['id'] ?>&page=<?php echo basename($_SERVER['PHP_SELF']) ?>">
				<li  class="inline-items">
					<div class="author-thumb">
						<img width="40" height="40" alt="author" src="<?php echo $informationssamis['avatar']; ?>" class="avatar">
						<span class="icon-status online"></span>
					</div>
				</li>
			</a>
<?php
}
?>

			</ul>
		</div>
		<div class="search-friend inline-items">
			<a href="#" class="js-sidebar-open">
				<svg class="menu-icon"><use xlink:href="icons/icons.svg#menu-icon"></use></svg>
			</a>
		</div>

		<a href="messages.php" class="chat inline-items">
			<svg class="chat---messages-icon"><use xlink:href="icons/icons.svg#chat---messages-icon"></use></svg>
		</a>

	</div>

	<div class="fixed-sidebar-right sidebar--large" id="sidebar-right-1">

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="chat-users">

<?php
$amischat = mysqli_query($con, "SELECT * FROM amis WHERE id_2='$id_log' OR id_1='$id_log'");
$numsami = $amischat->num_rows;
for ($s = 0;$s < $numsami;++$s)
{
    $sontamis = $amischat->fetch_array(MYSQLI_ASSOC);
    $id_1amis = $sontamis['id_1'];
    $id_2amis = $sontamis['id_2'];

    if ($sontamis['id_2'] == $id_log)
    {
        $infosamis = mysqli_query($con, "SELECT * FROM membres WHERE id='$id_1amis'");
        $informationssamis = $infosamis->fetch_array(MYSQLI_ASSOC);
    }
    else
    {
        $infosamis = mysqli_query($con, "SELECT * FROM membres WHERE id='$id_2amis'");
        $informationssamis = $infosamis->fetch_array(MYSQLI_ASSOC);
    }
?>

<style type="text/css">
    #upload{
    display: none;
}
    #uploadpubli{
    display: none;
}




.typeahead, .tt-query, .tt-hint,.recherche_amis {
	border: 2px solid #CCCCCC;
	border-radius: 8px;
	font-size: 40px;
	height: 10px;
	line-height: 30px;
	outline: medium none;
	padding: 8px 12px;
	width: 396px;
}

.typeahead {
	background-color: #4a4c63;
}
.typeahead:focus {
	border: 2px solid #0097CF;
}
.tt-query {
	box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
}
.tt-hint {
	color: #999999;
}
.tt-dropdown-menu {
	background-color: #4a4c63;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-radius: 8px;
	box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	margin-top: 0px;
	padding: 8px 0;
	width: 422px;
}

.tt-suggestion {
	font-size: 2px;
	line-height: 24px;
	padding: 3px 20px;
}
.tt-suggestion.tt-is-under-cursor {
	background-color: #0097CF;
	color: #FFFFFF;
}
.tt-suggestion p {
	margin: 0;
}
</style>	
	
				<li  class="inline- js-chat-open">
					<a href="box.php?id=<?php echo $informationssamis['id'] ?>&page=<?php echo basename($_SERVER['PHP_SELF']) ?>">
						<div class="author-thumb">
							<img width="40" height="40" alt="author" src="<?php echo $informationssamis['avatar']; ?>" class="avatar">
							<span class="icon-status online"></span>
						</div>
						<div class="author-status">
							<a href="#" class="h6 author-name"><?php echo $informationssamis['nom'] . ' ' . $informationssamis['prenom']; ?></a>
						</div>
					</a>
				</li>
<?php
}
?>
		
			</ul>
		</div>
		
		<div class="search-friend inline-items">
			<form method="POST" action="recherche.php" class="form-group with-button">
				<div class="button">
					<input type="text" name="recherche_amis" id="recherche_amis" class="recherche_amis" autocomplete="on" spellcheck="false" placeholder="Chercher des amis..">
					<button>
						<svg class="magnifying-glass-icon"><use xlink:href="icons/icons.svg#magnifying-glass-icon"></use></svg>
					</button>
				</div>
			</form>
		</div>

		<a href="" class="chat inline-items">
			<h6 class="chat-title">MYSOCIALNETWORK CHAT</h6>
			<svg class="chat---messages-icon"><use xlink:href="icons/icons.svg#chat---messages-icon"></use></svg>
		</a>
	</div>
</div>

<!-- ... fin Barre latérale fixe droite -->



<!-- entête -->
   
<header class="header" id="site-header">

	<div class="page-title">
		<h6>My Social Network</h6>
	</div>

	<div class="header-content-wrapper">
		<!--recherche de membres -->
		<form class="search-bar w-search notification-list friend-requests" method="POST" action="recherche.php" >
			<div class="form-group with-button">
     			<input type="text" name="typeahead" id="typeahead" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Rechercher une personne..">	
					<button>
						<svg class="magnifying-glass-icon"><use xlink:href="icons/icons.svg#magnifying-glass-icon"></use></svg>
					</button>
			</div>
		</form>
<?php
$requetes = mysqli_query($con, "SELECT * FROM requete WHERE id_receveur='$id_log'");
$nb_requetes = $requetes->num_rows;
?>
	<!-- notification requete d'amis -->					
	<div class="control-block">
		<div class="control-icon more has-items">
			<svg class="happy-face-icon"><use xlink:href="icons/icons.svg#happy-face-icon"></use></svg>
				<div class="label-avatar bg-blue"><?php echo $nb_requetes; ?></div>
				<div class="more-dropdown more-with-triangle triangle-top-center">
					<div class="ui-block-title ui-block-title-small">
						<h6 class="title">REQUETTES D'AMIS</h6>

					</div>

					<div class="mCustomScrollbar" data-mcs-theme="dark">
						<ul class="notification-list friend-requests">
												
<?php

for ($l = 0;$l < $nb_requetes;++$l)
{

    $info_requette = $requetes->fetch_array(MYSQLI_ASSOC);

    $id_emetteur = $info_requette['id_auteur'];
    $actif = $info_requette['accepter'];

    $recup_nom = mysqli_query($con, "SELECT id,nom,prenom,avatar FROM membres WHERE id='$id_emetteur'");

    $getnom = $recup_nom->fetch_array(MYSQLI_ASSOC);

    if ($actif == 0)
    {

?>	
							<li>
								<div class="author-thumb">
									<img style='height: 100%; width: 100%; object-fit: contain' src="<?php echo $getnom['avatar']; ?>" alt="author">
								</div>
								<div class="notification-event">
									<a href="/home.php?id=<?php echo $id_emetteur; ?>" class="h6 notification-friend"><?php echo $getnom['nom'] . ' ' . $getnom['prenom']; ?></a>
								</div>
									
										<a href="requetes.php?action=accepter&id_emmetteur=<?php echo $id_emetteur ?>&id_receveur=<?php echo $id_log ?>" class="accept-request">
											<span class="icon-add without-text">
												<svg class="happy-face-icon"><use xlink:href="icons/icons.svg#happy-face-icon"></use></svg>
											</span>
										</a>

										<a href="requetes.php?action=decliner&id_emmetteur=<?php echo $id_emetteur ?>&id_receveur=<?php echo $id_log ?>" class="accept-request request-del">
											<span class="icon-minus">
												<svg class="happy-face-icon"><use xlink:href="icons/icons.svg#happy-face-icon"></use></svg>
											</span>
										</a>
									
							</li>
<?php
    }
    else
    {
?>
							<li class="accepted">
								<div class="author-thumb">
									<img width="50" height="50" src="<?php echo $getnom['avatar']; ?>" alt="author">
								</div>
								<div class="notification-event">
									Vous et <a href="/home.php?id=<?php echo $id_emetteur; ?>" class="h6 notification-friend"><?php echo $getnom['nom'] . ' ' . $getnom['prenom']; ?></a> êtes amis. Ecrire sur <a href="/home.php?id=<?php echo $id_emetteur; ?>" class="notification-link">son profil</a>.
								</div>
							</li>
<?php
    }

}

?>
						</ul>
					</div>	
				</div>
			</div>
	

<?php
$mchat = mysqli_query($con, "SELECT * FROM messages WHERE mp_receveur='$id_log'");
$nb_mchat = $mchat->num_rows;
?>


			<div class="control-icon more has-items">
				<svg class="chat---messages-icon"><use xlink:href="icons/icons.svg#chat---messages-icon"></use></svg>
					<div class="label-avatar bg-purple"><?php echo $nb_mchat; ?></div>
					<div class="more-dropdown more-with-triangle triangle-top-center">
						<div class="ui-block-title ui-block-title-small">
							<h6 class="title">Chat / Messages</h6>
						</div>

					<div class="mCustomScrollbar" data-mcs-theme="dark">
						<ul class="notification-list chat-message">


<?php

$first = 1;
$messagepersonne = array ();
	$present = 0;
$ancienexpediteur = 0;
for ($l = 0;$l < $nb_mchat;++$l)
{



    $info_chat = $mchat->fetch_array(MYSQLI_ASSOC);

    $mp_expediteur = $info_chat['mp_expediteur'];

    $recup_nom = mysqli_query($con, "SELECT id,nom,prenom,avatar FROM membres WHERE id='$mp_expediteur'");

    $getnom = $recup_nom->fetch_array(MYSQLI_ASSOC);


	$recup_message = mysqli_query($con,"SELECT mp_receveur,mp_texte,mp_time FROM messages WHERE mp_expediteur='$mp_expediteur' AND mp_receveur='$id_log' ORDER BY mp_time DESC");
	$message_chat= $recup_message->fetch_array(MYSQLI_ASSOC);

$ancienexpediteur = $mp_expediteur;

if ($first == 1)
{
	
}
else
{
foreach($messagepersonne as $element)
{
    if($element != $ancienexpediteur)
    {
    	$present = $present + 1;
    } 
}
	$ancienexpediteur = $mp_expediteur;
}
	
if ( count($messagepersonne) == $present || $first == 1) 
{
$messagepersonne[] = $mp_expediteur;
								
?>

							<li>
								<div class="author-thumb">
									<img style='height: 100%; width: 100%; object-fit: contain' src="<?php echo $getnom['avatar']; ?>" alt="author">
								</div>
								<div class="notification-event">								
									<a href="box.php?id=<?php echo $getnom['id'] ?>&page=<?php echo basename($_SERVER['PHP_SELF']) ?>" class="h6 notification-friend"><?php echo $getnom['nom'] . ' ' . $getnom['prenom']; ?></a>
									<span class="chat-message-item"><?php echo $message_chat['mp_texte'];?></span>
									<span class="notification-date"><time class="entry-date updated" datetime=""><?php echo date('Y-m-d H:i:s', $message_chat['mp_time'] + 21600); //heure fr ?></time></span>
								</div>
								<span class="notification-icon">
									<svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
								</span>

								<div class="more">
									<svg class="olymp-three-dots-icon"><use xlink:href="icons/icons.svg#olymp-three-dots-icon"></use></svg>
								</div>
							</li>

<?php
$first = 0;
}
}

?>



						</ul>
					</div>
				</div>
			</div>		

			

			<!-- Notifications messages -->


			<div class="control-icon more has-items">
				<svg class="thunder-icon"><use xlink:href="icons/icons.svg#thunder-icon"></use></svg>

				<div class="label-avatar bg-primary"><?php echo $nb_mchat; ?></div>

				<div class="more-dropdown more-with-triangle triangle-top-center">
					<div class="ui-block-title ui-block-title-small">
						<h6 class="title">Notifications</h6>	
					</div>

					<div class="mCustomScrollbar" data-mcs-theme="dark">
						<ul class="notification-list">


<?php

for ($g = 0;$g < $nb_mchat;++$g)
{
	$mchat2 = mysqli_query($con, "SELECT * FROM messages WHERE mp_receveur='$id_log'");

    $info_chat2 = $mchat2->fetch_array(MYSQLI_ASSOC);

    $mp_expediteur = $info_chat2['mp_expediteur'];

    $recup_nom2 = mysqli_query($con, "SELECT id,nom,prenom,avatar FROM membres WHERE id='$mp_expediteur'");

    $getnom2 = $recup_nom2->fetch_array(MYSQLI_ASSOC);


								
?>


													<li>
							<div class="author-thumb">
								<img style='height: 100%; width: 100%; object-fit: contain' src="<?php echo $getnom2['avatar']; ?>" alt="author">
							</div>
							<div class="notification-event">
								<a href="box.php?id=<?php echo $mp_expediteur; ?>&page=<?php echo basename($_SERVER['PHP_SELF']) ?>" class="h6 notification-friend"><?php echo $getnom2['nom'] . ' ' . $getnom2['prenom']; ?></a> vous a envoyé un message ! 
								
								</a>
							</div>
							<span class="notification-icon">
								<svg class="chat---messages-icon"><use xlink:href="icons/icons.svg#chat---messages-icon"></use></svg>
							</span>

							<div class="more">
								<svg class="three-dots-icon"><use xlink:href="icons/icons.svg#three-dots-icon"></use></svg>
							</div>
						</li>

<?php
}

?>

						</ul>
					</div>

					
				</div>
			</div>

			<div class="author-page author vcard inline-items more">
				<div class="author-thumb">
				
					<img alt="author" width="42" height="42" src="<?php echo $getinfosperso['avatar']; ?>" class="avatar">
					<span class="icon-status online"></span>
					<div class="more-dropdown more-with-triangle">
						<div class="mCustomScrollbar" data-mcs-theme="dark">
							<div class="ui-block-title ui-block-title-small">
								<h6 class="title">Votre compte</h6>
							</div>

							<ul class="account-settings">
								<li>
									<a href="home.php?id=<?php echo $id_log; ?>">
										<svg class="star-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="FAV PAGE"><use xlink:href="icons/icons.svg#star-icon"></use></svg>
										<span>Votre profil</span>
									</a>
								</li>
								<li>
									<a href="parametres.php">
										<svg class="menu-icon"><use xlink:href="icons/icons.svg#menu-icon"></use></svg>
										<span>Parametres du profil</span>
									</a>
								</li>
								<li>
									<a href="deconnexion.php">
										<svg class="logout-icon"><use xlink:href="icons/icons.svg#logout-icon"></use></svg>
										<span>Deconnexion</span>
									</a>
								</li>
							</ul>

							<div class="ui-block-title ui-block-title-small">
								<h6 class="title">Chat</h6>
							</div>
							<!-- Statut de connexion, pas encore fonctionnel -->
							<ul class="chat-settings">
								<li>
									<a href="#">
										<span class="icon-status online"></span>
										<span>En ligne</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="icon-status away"></span>
										<span>Absent</span>
									</a>
								</li>

								<li>
									<a href="#">
										<span class="icon-status status-invisible"></span>
										<span>Invisible</span>
									</a>
								</li>
							</ul>

							<!-- Statu à définir par l'utilisateur -->
							<div class="ui-block-title ui-block-title-small">
								<h6 class="title">Status</h6>
							</div>

							<form class="form-group with-button custom-status" method='post' action='parametres.php'>
								<input class="form-control" placeholder="" name="statupterso" type="text" value="<?php echo $getinfosperso['statut']; ?>">

								<button class="bg-purple" type="submit" name="action" value="statut" >
									<svg class="check-icon"><use xlink:href="icons/icons.svg#check-icon"></use></svg>
								</button>
							</form>

							<ul>
								<li>
									<a href="#">
										<span>Contact</span>
									</a>
								</li>
							</ul>
						</div>

					</div>
				</div>
				<a href="home.php?id=<?php echo $id_log; ?>" class="author-name fn">
					<div class="author-title">
						<?php echo $userstr; ?> <svg class="dropdown-arrow-icon"><use xlink:href="icons/icons.svg#dropdown-arrow-icon"></use></svg>
					</div>
					<span class="author-subtitle"><?php echo $getinfosperso['statut']; ?> </span>
				</a>
			</div>

		</div>
	</div>

</header>

<!-- ... fin entête -->

			



<!-- ... fin Fenêtre-popup  -->

		<!-- jQuery first, then Other JS. -->
<script src="js/jquery-3.2.0.min.js"></script>
<script src="js/typeahead.min.js"></script>
<!-- Js effects for material design. + Tooltips -->
<script src="js/material.min.js"></script>
<!-- Helper scripts (Tabs, Equal height, Scrollbar, etc) -->
<script src="js/theme-plugins.js"></script>
<!-- Init functions -->
<script src="js/main.js"></script>

<script src="js/jquery.magnific-popup.min.js"></script>


<script>
	    $(document).ready(function(){
    $('input.typeahead').typeahead({
        name: 'typeahead',
        remote:'search.php?key=%QUERY',
        limit : 10
    });
		    
    	
});

</script>

<script>
	    $(document).ready(function(){
$('input.recherche_amis').typeahead({
  name: 'recherche_amis',
  remote : 'search.php?id=<?php echo $id_log; ?>&amiami=%QUERY'
});
		    
    	
});


</script>



</body>
</html>
