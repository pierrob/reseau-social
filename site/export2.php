<!-- BONIJOL Pierre et LECOCQ Coraline -->
<!-- Génération du fichier xml, incluant la dtd et le fichier xsl version 2 -->
<?php

$con = mysqli_connect("sql313.unaux.com", "unaux_21621064", "s0t5nwpd", "unaux_21621064_social");

if (isset($_GET['action']))
{
    $idpage = $_GET['id'];

    $filename = $_GET['file'];

    $sql = mysqli_query($con, "SELECT id,nom,prenom,datetimepicker,sexe,description,hobbies, pays, musique, serie, statut  FROM membres WHERE id='$idpage'");

    $imp = new DOMImplementation;

    // Création d'une instance DOMDocumentType
    $dtd = $imp->createDocumentType('informations', '', 'mysocialnetwork.dtd');

    // Création d'une instance DOMDocument
    $xml = $imp->createDocument("", "", $dtd);

    // Définition des autres propriétés
    $xml->encoding = 'UTF-8';
    $xml->standalone = false;

    $xml->preserveWhiteSpace = false;
    $xml->formatOutput = true;

    $xslt = $xml->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="design2.xsl"');

    $xml->appendChild($xslt);

	/*racine*/
    $root = $xml->createElement("informations");

    $table = mysqli_fetch_assoc($sql);
	
	/*toutes les balises du fichier*/
    $node = $xml->createElement("nom", $table['nom']);
    $root->appendChild($node);

    $node = $xml->createElement("prenom", $table['prenom']);
    $root->appendChild($node);

    $node = $xml->createElement("sexe", $table['sexe']);
    $root->appendChild($node);

    $node = $xml->createElement("anniversaire", $table['datetimepicker']);
    $root->appendChild($node);

    $node = $xml->createElement("pays", $table['pays']);
    $root->appendChild($node);

    $node = $xml->createElement("statut", $table['statut']);
    $root->appendChild($node);

    $node = $xml->createElement("description", $table['description']);
    $root->appendChild($node);

    $node = $xml->createElement("musiques", $table['musique']);
    $root->appendChild($node);

    $node = $xml->createElement("series", $table['serie']);
    $root->appendChild($node);

    $node = $xml->createElement("hobbies", $table['hobbies']);
    $root->appendChild($node);

    /*fin racine*/
    $xml->appendChild($root);

	/*enregistrement*/
    $xml->saveXML();

    if ($_GET['action'] == 'getinfos')
    {

        // téléchargement du fichier xsl version 2
        $xsl = new DOMDocument;
        $xsl->load('design2.xsl');

        $proc = new XSLTProcessor;

        $proc->importStyleSheet($xsl);

        echo $proc->transformToXML($xml);

    }
    else
    {
        echo $xml->saveXML();
    }

}

if (isset($_GET['action']) && $_GET['action'] == 'exporter')
{

    header('Content-Disposition: attachment; filename="' . $filename . '"');
    header("Content-Type: text/xml");
}

if ($_GET['action'] == 'getinfos')
{

?>

	<a href="export2.php?action=exporter&file=<?php echo $filename; ?>&id=<?php echo $idpage; ?>" >Télécharger le fichier xml contenant ces informations</a>
	<br/>

<?php
    echo "Retourner <a href='{$_SERVER['HTTP_REFERER']}'> en arriere</a>";
}

mysqli_close($con);

?>
